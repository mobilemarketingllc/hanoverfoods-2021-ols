<?php
/*
Plugin Name: uRemediate Plugin
Plugin URI: https://www.user1st.com/uremediate
Description: Enable accessibility across your WordPress site. Empower your users by offering accommodations that fit their individual needs.
Version: 3.0
Author: User1st
Author URI: https://www.user1st.com/
*/

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}
define('USER1ST_USUITE_PLUGIN_VERSION', '2.0');

add_action('wp_enqueue_scripts', 'user1st_usuite_plugin_enqueue_scripts');
function user1st_usuite_plugin_enqueue_scripts()
{
    // do wp_enqueue_script in order to be able to add a inline script to it (WP best practices)
    wp_enqueue_script('user1st_usuite_plugin', plugins_url('/public/usuite-plugin.js', __FILE__));

    $settings = get_option('user1st_usuite_plugin_settings');
    $json = [];
    if (!empty($settings)) {
        if (!empty($settings['hide_the_accessibility_button_flag']))
            $json['initHidden'] = true;
        if (!empty($settings['your_own_html_element_with_your_own_styles']))
            $json['accessibilityBtn']['useMy'] = $settings['your_own_html_element_with_your_own_styles'];
        if (!empty($settings['default_html_element_with_your_own_styles']))
            $json['accessibilityBtn']['useMyClass'] = $settings['default_html_element_with_your_own_styles'];
        if (!empty($settings['disable_the_fallback_button_flag']))
            $json['accessibilityBtn']['disableFallbackButton'] = true;
        if (!empty($settings['custom_duration_for_the_tooltip']))
            $json['profileInstructions'] = ['duration' => $settings['custom_duration_for_the_tooltip']];
        if (!empty($settings['highlighter_color']))
            $json['highlighter'] = ['color' => $settings['highlighter_color']];
        if (!empty($settings['google_analytics_account'])){
            $json['analytics'] = [
                'name' => 'create',
                'trace' => '[RAW]function(traceItem){ trace(traceItem); }[RAW]'
            ];
            $json['tracing'] = ['gaAccount' => $settings['google_analytics_account']];
        }
    }
    $_u1stSettings = json_encode($json);
    $_u1stSettings = str_replace(['"[RAW]', '[RAW]"'], ' ', $_u1stSettings);

    $inline_trace_js = 'function trace(e){for(var s=function(e,s){return["_trackEvent","User1st",e,s]},a=function(e){var a=[],r="User1st.FE.";switch(e.name.toLowerCase()){case"mainmenu.open":a.push(s(r+"MainMenu.Open",""));break;case"mainmenu.close":a.push(s(r+"MainMenu.Close",""));break;case"sys.profileselected":a.push(s(r+"Sys.ProfileSelected",e.values.profileID));break;case"sys.featureschanged":for(var u=e.values.removedFeatures.split("|"),n=0;n<u.length;n++)a.push(s(r+"Sys.feature-TurnOff",u[n]));for(var t=e.values.selectedFreatures.split("|"),n=0;n<t.length;n++)a.push(s(r+"Sys.feature-TurnOn",t[n]));break;case"sys.userfeedback":keyValuePairs.push(r+"Sys.UserFeedback",e.values.type+" - "+e.values.content)}return a}(e),r=0;r<a.length;r++)_gaq.push(a[r])}';
    $inline_js = 'var _u1stSettings =  ' . $_u1stSettings . ';
                        var isActive = ((/u1stIsActive=1/).test(document.cookie));
                        var script = document.createElement("script");
                        script.id = "User1st_Loader";
                        script.src = "https://fecdn.user1st.info/Loader/head";
                        (!isActive) && (script.async ="true");
                        var documentPosition = document.head || document.documentElement;
                        documentPosition.insertAdjacentElement("afterbegin", script);';
    wp_add_inline_script('user1st_usuite_plugin', $inline_js);
}

if (is_admin()) {
    require_once(plugin_dir_path(__FILE__) . 'class.usuite-plugin.admin.php');
    $uSuitePluginAdminSettings = new uSuitePluginAdminSettings();
    $plugin = plugin_basename(__FILE__);
    add_filter('plugin_action_links_' . $plugin, 'user1st_usuite_plugin_settings_link');
}
function user1st_usuite_plugin_settings_link($links)
{
    $settings_link = '<a href="' . esc_url(admin_url('options-general.php?page=user1st-usuite-plugin-settings')) . '">Settings</a>';
    array_unshift($links, $settings_link);
    return $links;
}
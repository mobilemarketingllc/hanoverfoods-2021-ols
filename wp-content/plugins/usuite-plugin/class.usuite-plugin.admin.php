<?php

class uSuitePluginAdminSettings
{
    private $options;

    public function __construct()
    {
        add_action('admin_menu', [$this, 'add_plugin_page']);
        add_action('admin_init', [$this, 'page_init']);
    }

    public function add_plugin_page()
    {
        add_options_page(
            'Settings',
            'User1st uSuite Plugin',
            'manage_options',
            'user1st-usuite-plugin-settings',
            [$this, 'create_admin_page']
        );
    }

    public function create_admin_page()
    {
        $this->options = get_option('user1st_usuite_plugin_settings');
        ?>
        <div class="wrap">
            <h1>User1st uSuite Plugin Settings</h1>
            <form method="post" action="options.php">
                <?php
                settings_fields('user1st_usuite_plugin');
                do_settings_sections('user1st-usuite-plugin-settings');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    public function page_init()
    {
        register_setting(
            'user1st_usuite_plugin',
            'user1st_usuite_plugin_settings',
            [$this, 'sanitize']
        );
        add_settings_section(
            'default',
            'General Settings',
            function () {
            },
            'user1st-usuite-plugin-settings'
        );
        add_settings_field(
            'hide_the_accessibility_button_flag',
            'Hide the Accessibility Button:',
            [$this, 'hide_the_accessibility_button_flag_callback'],
            'user1st-usuite-plugin-settings'
        );
        add_settings_field(
            'your_own_html_element_with_your_own_styles',
            'Use your own HTML Element with your own styles:',
            [$this, 'your_own_html_element_with_your_own_styles_callback'],
            'user1st-usuite-plugin-settings'
        );
        add_settings_field(
            'default_html_element_with_your_own_styles',
            'Use the default HTML Element with your own styles:',
            [$this, 'default_html_element_with_your_own_styles_callback'],
            'user1st-usuite-plugin-settings'
        );
        add_settings_field(
            'disable_the_fallback_button_flag',
            'Disable the Fallback Button:',
            [$this, 'disable_the_fallback_button_flag_callback'],
            'user1st-usuite-plugin-settings'
        );
        add_settings_field(
            'custom_duration_for_the_tooltip',
            'Custom Duration for Displaying Tooltip Instructions:',
            [$this, 'custom_duration_for_the_tooltip_callback'],
            'user1st-usuite-plugin-settings'
        );
        add_settings_field(
            'highlighter_color',
            'Highlighter Color:',
            [$this, 'highlighter_color_callback'],
            'user1st-usuite-plugin-settings'
        );
        add_settings_field(
            'google_analytics_account',
            'Google Analytics Account:',
            [$this, 'google_analytics_account_callback'],
            'user1st-usuite-plugin-settings'
        );
    }

    public function sanitize($input)
    {
        $sanitized_input = [];

        if (!empty($input['hide_the_accessibility_button_flag']))
            $sanitized_input['hide_the_accessibility_button_flag'] = sanitize_text_field($input['hide_the_accessibility_button_flag']);
        if (!empty($input['your_own_html_element_with_your_own_styles']))
            $sanitized_input['your_own_html_element_with_your_own_styles'] = sanitize_text_field($input['your_own_html_element_with_your_own_styles']);
        if (!empty($input['default_html_element_with_your_own_styles']))
            $sanitized_input['default_html_element_with_your_own_styles'] = sanitize_text_field($input['default_html_element_with_your_own_styles']);
        if (!empty($input['disable_the_fallback_button_flag']))
            $sanitized_input['disable_the_fallback_button_flag'] = sanitize_text_field($input['disable_the_fallback_button_flag']);
        if (!empty($input['custom_duration_for_the_tooltip']))
            $sanitized_input['custom_duration_for_the_tooltip'] = sanitize_text_field($input['custom_duration_for_the_tooltip']);
        if (!empty($input['highlighter_color']))
            $sanitized_input['highlighter_color'] = sanitize_text_field($input['highlighter_color']);
        if (!empty($input['google_analytics_account']))
            $sanitized_input['google_analytics_account'] = sanitize_text_field($input['google_analytics_account']);

        return $sanitized_input;
    }

    public function hide_the_accessibility_button_flag_callback()
    {
        printf(
            '<input type="checkbox" id="hide_the_accessibility_button_flag" name="user1st_usuite_plugin_settings[hide_the_accessibility_button_flag]" %s />',
            !empty($this->options['hide_the_accessibility_button_flag']) ? 'checked' : ''
        );

    }

    public function your_own_html_element_with_your_own_styles_callback()
    {
        printf(
            '<input type="text" class="regular-text" id="your_own_html_element_with_your_own_styles" name="user1st_usuite_plugin_settings[your_own_html_element_with_your_own_styles]" value="%s" /><p class="description">provide your button ID</p>',
            !empty($this->options['your_own_html_element_with_your_own_styles']) ? esc_attr($this->options['your_own_html_element_with_your_own_styles']) : ''
        );
    }

    public function default_html_element_with_your_own_styles_callback()
    {
        printf(
            '<input type="text" class="regular-text" id="default_html_element_with_your_own_styles" name="user1st_usuite_plugin_settings[default_html_element_with_your_own_styles]" value="%s" /><p class="description">provide your class name</p>',
            !empty($this->options['default_html_element_with_your_own_styles']) ? esc_attr($this->options['default_html_element_with_your_own_styles']) : ''
        );
    }

    public function disable_the_fallback_button_flag_callback()
    {
        printf(
            '<input type="checkbox" id="disable_the_fallback_button_flag" name="user1st_usuite_plugin_settings[disable_the_fallback_button_flag]" %s />',
            !empty($this->options['disable_the_fallback_button_flag']) ? 'checked' : ''
        );
    }

    public function custom_duration_for_the_tooltip_callback()
    {
        printf(
            '<input type="text" class="regular-text" id="custom_duration_for_the_tooltip" name="user1st_usuite_plugin_settings[custom_duration_for_the_tooltip]" value="%s" /><p class="description">e.g. 2000 (milliseconds)</p>',
            !empty($this->options['custom_duration_for_the_tooltip']) ? esc_attr($this->options['custom_duration_for_the_tooltip']) : ''
        );
    }

    public function highlighter_color_callback()
    {
        printf(
            '<input type="text" class="regular-text" id="highlighter_color" name="user1st_usuite_plugin_settings[highlighter_color]" value="%s" /><p class="description">e.g. rgb(0,0,0)</p>',
            !empty($this->options['highlighter_color']) ? esc_attr($this->options['highlighter_color']) : ''
        );
    }

    public function google_analytics_account_callback()
    {
        printf(
            '<input type="text" class="regular-text" id="google_analytics_account" name="user1st_usuite_plugin_settings[google_analytics_account]" value="%s" /><p class="description">e.g. UA-XXXXXX-XX</p>',
            !empty($this->options['google_analytics_account']) ? esc_attr($this->options['google_analytics_account']) : ''
        );
    }
}
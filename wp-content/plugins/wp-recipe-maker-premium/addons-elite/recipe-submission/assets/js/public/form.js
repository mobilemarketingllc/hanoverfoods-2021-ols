// import * as FilePond from 'filepond';
// import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
// import 'filepond/dist/filepond.min.css';
// import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
// import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
// import FilePondPluginFileEncode from 'filepond-plugin-file-encode';

// FilePond.registerPlugin(FilePondPluginFileValidateSize);
// FilePond.registerPlugin(FilePondPluginImagePreview);
// FilePond.registerPlugin(FilePondPluginFileEncode);

import '../../css/public/form.scss';
import '../../css/public/blocks.scss';

window.WPRecipeMaker.submission = {
	init() {
        // TODO. Problem with actually uploading the files. Use regular HTML5 form for now.
        // const images = document.querySelectorAll( '.wprmprs-layout-block-recipe_image' );
        // const maxFileSize = window.WPRecipeMaker.submission.formatBytes( wprmp_public.recipe_submission.max_file_size );

        // for ( let image of images ) {
        //     const input = image.querySelector( 'input' );
        //     const placeholder = input.dataset.placeholder;

        //     FilePond.create( input, {            
        //         labelIdle: placeholder,
        //         maxFiles: 1,
        //         maxFileSize: maxFileSize,
        //         labelMaxFileSizeExceeded: wprmp_public.recipe_submission.text.image_size,
        //         labelMaxFileSize: `> {filesize}`,
        //     } );
        // }
    },
    onSubmit( form ) {
        // Already submitting, prevent double.
        if (  form.classList.contains('is-submitting') ) {
            return false;
        }

        // Prevent double submission
        form.classList.add('is-submitting');

        return true;
    },
    formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';
    
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    
        const i = Math.floor(Math.log(bytes) / Math.log(k));
    
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + '' + sizes[i];
    },
};

ready(() => {
	window.WPRecipeMaker.submission.init();
});

function ready( fn ) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
<div class="wprmprs-layout-block-submit">
    <input type="submit" id="wprmprs_submit" name="wprmprs_submit" class="button" value="<?php echo esc_attr( do_shortcode( $block['text'] ) ); ?>" />
</div>
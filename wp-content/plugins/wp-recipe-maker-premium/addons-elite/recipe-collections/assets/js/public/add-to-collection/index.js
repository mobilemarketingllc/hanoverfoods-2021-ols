const AddToCollection = {
    checkInbox: (recipeId) => {
        const localCollections = localStorage.getItem( 'wprm-recipe-collection' );

        if ( localCollections ) {
            const collections = JSON.parse(localCollections);
            const matches = collections.inbox.items['0-0'].filter((item) => item.recipeId === recipeId );
            
            if ( matches.length > 0 ) {
                AddToCollection.updateButtons(recipeId);
            }
        }
    },
    updateButtons: (recipeId) => {
        const buttons = document.querySelectorAll( '.wprm-recipe-add-to-collection' );

        for ( let button of buttons ) {
            if ( recipeId === parseInt( button.dataset.recipeId ) ) {
                if ( button.classList.contains( 'wprm-recipe-not-in-collection' ) ) {
                    button.style.display = 'none';
                } else if ( button.classList.contains( 'wprm-recipe-in-collection' ) ) {
                    button.style.display = '';
                }
            }
        }
    }
}

export default AddToCollection;

window.WPRecipeMaker.collections = {
	init() {
        // Check local storage for recipe if user is not logged in.
        if ( 0 === parseInt( wprmp_public.user ) ) {
            const buttons = document.querySelectorAll( '.wprm-recipe-add-to-collection.wprm-recipe-not-in-collection' );

            for ( let button of buttons ) {
                const recipeId = parseInt( button.dataset.recipeId );
                AddToCollection.checkInbox( recipeId );
            }
        }

        // Check for click.
        document.addEventListener( 'click', function(e) {
			for ( var target = e.target; target && target != this; target = target.parentNode ) {
				if ( target.matches( '.wprm-recipe-add-to-collection.wprm-recipe-not-in-collection' ) ) {
					window.WPRecipeMaker.collections.onClick( target, e );
					break;
				}
			}
		}, false );
    },
    onClick( el, e ) {
        e.preventDefault();
        const recipeId = parseInt( el.dataset.recipeId );

        const servingsContainers = document.querySelectorAll( '.wprm-recipe-servings-' + recipeId );
        let servings = 0 < servingsContainers.length ? servingsContainers[0].dataset.servings : false;
        if ( isNaN( servings ) ) {
            servings = false;
        }

        // Add to inbox.
        if ( 0 === parseInt( wprmp_public.user ) ) {
            const localCollections = localStorage.getItem( 'wprm-recipe-collection' );

            let collections;
            if ( localCollections ) {
                collections = JSON.parse(localCollections);
            } else {
                collections = wprmp_public.collections.default;
            }

            const recipe = JSON.parse( el.dataset.recipe );

            // Get unique ID for recipe.
            let maxId = Math.max.apply( Math, collections.inbox.items['0-0'].map( function(item) { return item.id; } ) );
            maxId = maxId < 0 ? -1 : maxId;
            recipe.id = maxId + 1;

            if ( false !== servings ) {
                recipe.servings = servings;
            }

            collections.inbox.nbrItems++;
            collections.inbox.items['0-0'].push(recipe);

            localStorage.setItem( 'wprm-recipe-collection', JSON.stringify( collections ) );
        } else {
            fetch(`${wprmp_public.endpoints.collections_helper}/inbox`, {
                method: 'POST',
                headers: {
                    'X-WP-Nonce': wprm_public.api_nonce,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                credentials: 'same-origin',
                body: JSON.stringify({
                    recipeId,
                    servings,
                }),
            });
        }
        
        // Update all buttons for this recipe.
        AddToCollection.updateButtons(recipeId);
    },
};

ready(() => {
	window.WPRecipeMaker.collections.init();
});

function ready( fn ) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
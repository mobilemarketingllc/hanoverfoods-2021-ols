import React, { Component } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import Select from 'react-select';

import { __wprm } from 'Shared/Translations';

import Item from '../../Item';

export default class AddNote extends Component {
    componentDidMount() {
        this.props.onChangeAddItems([
            {
                type: 'note',
                name: '',
                color: 'none',
            }
        ]);
    }

    render() {
        const addInterface = this.props.hasOwnProperty( 'interface' ) ? this.props.interface : 'drag';
        const item = {
            ...this.props.addItems[0],
            id: 'select-0',
        }

        if ( 'note' !== item.type ) {
            return null;
        }

        const colorOptions = [
            { value: 'none', label: __wprm( 'None' ) },
            { value: 'blue', label: __wprm( 'Blue' ) },
            { value: 'red', label: __wprm( 'Red' ) },
            { value: 'green', label: __wprm( 'Green' ) },
            { value: 'yellow', label: __wprm( 'Yellow' ) },
        ];

        return (
            <div className="wprmprc-collection-action-add-note">
                <Droppable
                    droppableId={`select-items`}
                    type='RECIPE'
                    isDropDisabled={true}
                >
                    {(provided, snapshot) => (
                        <div
                            className="wprmprc-collection-action-select-items"
                            ref={provided.innerRef}
                            {...provided.droppableProps}
                        >
                            <div style={{padding: '0 5px 5px 5px', fontStyle: 'italic', fontSize: '0.8em'}}>
                                {
                                    'click' === addInterface
                                    ?
                                    __wprm( 'Click to add:' )
                                    :
                                    __wprm( 'Drag and drop to add:' )
                                }
                            </div>
                            <Item
                                type={this.props.type}
                                collection={this.props.collection}
                                item={ item }
                                interface={ this.props.interface }
                                onAddItem={ this.props.onAddItem }
                                key="select-custom"
                                index={ 0 }
                            />
                        </div>
                    )}
                </Droppable>
                <div className="wprmprc-collection-action-add-note-form">
                    <label htmlFor="wprmprc-collection-action-add-note-name">{ __wprm( 'Note' ) }</label>
                    <textarea
                        id="wprmprc-collection-action-add-note-name"
                        value={item.name}
                        onChange={(event) => 
                            this.props.onChangeAddItems([
                                {
                                    ...item,
                                    name: event.target.value,
                                }
                            ])
                        }
                    />
                    <label>{ __wprm( 'Color' ) }</label>
                    <Select
                        className="wprmprc-collection-action-add-item-color"
                        value={colorOptions.filter(({value}) => value === item.color)}
                        onChange={(option) =>
                            this.props.onChangeAddItems([
                                {
                                    ...item,
                                    color: option.value,
                                }
                            ])
                        }
                        options={colorOptions}
                        clearable={false}
                        styles={{
                            control: styles => ({ ...styles, borderRadius: 5 }),
                        }}
                    />
                </div>
            </div>
        );
    }
}

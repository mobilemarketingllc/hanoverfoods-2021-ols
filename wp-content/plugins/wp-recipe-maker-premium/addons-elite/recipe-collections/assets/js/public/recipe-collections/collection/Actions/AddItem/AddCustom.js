import React, { Component, Fragment } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import Select from 'react-select';

import { __wprm } from 'Shared/Translations';

import Item from '../../Item';
import EditList from '../../../general/EditList';

export default class AddCustom extends Component {
    componentDidMount() {
        this.props.onChangeAddItems([
            {
                type: 'ingredient',
                name: '',
                color: 'none',
                ingredients: [
                    {
                        id: 0,
                        amount: '',
                        unit: '',
                        name: '',
                    }
                ],
                nutrition: {}
            }
        ]);
    }

    render() {
        const addInterface = this.props.hasOwnProperty( 'interface' ) ? this.props.interface : 'drag';
        const item = {
            ...this.props.addItems[0],
            id: 'select-0',
        }

        if ( 'ingredient' !== item.type ) {
            return null;
        }

        const colorOptions = [
            { value: 'none', label: __wprm( 'None' ) },
            { value: 'blue', label: __wprm( 'Blue' ) },
            { value: 'red', label: __wprm( 'Red' ) },
            { value: 'green', label: __wprm( 'Green' ) },
            { value: 'yellow', label: __wprm( 'Yellow' ) },
        ];

        return (
            <div className="wprmprc-collection-action-add-ingredient">
                <Droppable
                    droppableId={`select-items`}
                    type='RECIPE'
                    isDropDisabled={true}
                >
                    {(provided, snapshot) => (
                        <div
                            className="wprmprc-collection-action-select-items"
                            ref={provided.innerRef}
                            {...provided.droppableProps}
                        >
                            <div style={{padding: '0 5px 5px 5px', fontStyle: 'italic', fontSize: '0.8em'}}>
                                {
                                    'click' === addInterface
                                    ?
                                    __wprm( 'Click to add:' )
                                    :
                                    __wprm( 'Drag and drop to add:' )
                                }
                            </div>
                            <Item
                                type={this.props.type}
                                collection={this.props.collection}
                                item={ item }
                                interface={ this.props.interface }
                                onAddItem={ this.props.onAddItem }
                                key="select-custom"
                                index={ 0 }
                            />
                        </div>
                    )}
                </Droppable>
                <div className="wprmprc-collection-action-add-ingredient-form">
                    <label htmlFor="wprmprc-collection-action-add-ingredient-name">{ __wprm( 'Name' ) }</label>
                    <input
                        id="wprmprc-collection-action-add-ingredient-name"
                        type="text"
                        value={item.name}
                        onChange={(event) => 
                            this.props.onChangeAddItems([
                                {
                                    ...item,
                                    name: event.target.value,
                                }
                            ])
                        }
                    />
                    <label>{ __wprm( 'Ingredients' ) }</label>
                    <EditList
                        type='ingredient'
                        onAdd={() => {
                            let items = [ ...item.ingredients ];
                            let maxId = Math.max.apply( Math, items.map( function(item) { return item.id; } ) );
                            maxId = maxId < 0 ? -1 : maxId;

                            items.push({ id: maxId + 1, amount: '', unit: '', name: '' });
                            this.props.onChangeAddItems([
                                {
                                    ...item,
                                    ingredients: items,
                                }
                            ])
                        }}
                        onDelete={(id, index) => {
                            let items = [ ...item.ingredients ];
                            items.splice(index, 1);

                            this.props.onChangeAddItems([
                                {
                                    ...item,
                                    ingredients: items,
                                }
                            ])
                        }}
                        onReorder={(newItems) => {
                            this.props.onChangeAddItems([
                                {
                                    ...item,
                                    ingredients: newItems,
                                }
                            ])
                        }}
                        items={item.ingredients}
                        item={(editing, ingredient, index) =>
                            <Fragment>
                                {
                                    editing
                                    ?
                                    <Fragment>
                                        <input
                                            type="text"
                                            value={ingredient.amount}
                                            placeholder="1"
                                            onChange={(event) => {
                                                let items = [ ...item.ingredients ];

                                                items[index] = {
                                                    ...items[index],
                                                    amount: event.target.value,
                                                }

                                                this.props.onChangeAddItems([
                                                    {
                                                        ...item,
                                                        ingredients: items,
                                                    }
                                                ])
                                            }}
                                        />
                                        <input
                                            type="text"
                                            value={ingredient.unit}
                                            placeholder={ __wprm( 'cup' ) }
                                            onChange={(event) => {
                                                let items = [ ...item.ingredients ];

                                                items[index] = {
                                                    ...items[index],
                                                    unit: event.target.value,
                                                }

                                                this.props.onChangeAddItems([
                                                    {
                                                        ...item,
                                                        ingredients: items,
                                                    }
                                                ])
                                            }}
                                        />
                                        <input
                                            type="text"
                                            value={ingredient.name}
                                            placeholder={ __wprm( 'olive oil' ) }
                                            onChange={(event) => {
                                                let items = [ ...item.ingredients ];

                                                items[index] = {
                                                    ...items[index],
                                                    name: event.target.value,
                                                }

                                                this.props.onChangeAddItems([
                                                    {
                                                        ...item,
                                                        ingredients: items,
                                                    }
                                                ])
                                            }}
                                        />
                                    </Fragment>
                                    :
                                    <Fragment>
                                        { `${ ingredient.amount ? `${ ingredient.amount } ` : '' } ${ ingredient.unit ? `${ ingredient.unit } ` : '' }${ ingredient.name }`.trim() }
                                    </Fragment>
                                }
                            </Fragment>
                        }
                        labels={{
                            add: __wprm( 'Add Ingredient' ),
                            edit: __wprm( 'Edit Ingredients' ),
                        }}
                        editing={ true }
                        skipConfirm={ true }
                    />
                    {
                        true === wprmprc_public.settings.recipe_collections_nutrition_facts
                        &&
                        <Fragment>
                            <label>{ __wprm( 'Nutrition Facts (per serving)' ) }</label>
                            {
                                wprmprc_public.settings.recipe_collections_nutrition_facts_fields.map((nutritionField, index) => {
                                    const value = item.nutrition.hasOwnProperty( nutritionField ) ? item.nutrition[ nutritionField ] : '';
                                    const label = wprmprc_public.labels.nutrition_fields.hasOwnProperty( nutritionField ) ? wprmprc_public.labels.nutrition_fields[ nutritionField ].label : nutritionField;
                                    const unit = wprmprc_public.labels.nutrition_fields.hasOwnProperty( nutritionField ) ? wprmprc_public.labels.nutrition_fields[ nutritionField ].unit : '';

                                    return (
                                        <div
                                            className="wprmprc-collection-action-add-ingredient-nutrient"
                                            key={index}
                                        >
                                            <div className="wprmprc-collection-action-add-ingredient-nutrient-value">
                                                <input
                                                    type="number"
                                                    value={ value }
                                                    onChange={(event) => {
                                                        let nutrition = {
                                                            ...item.nutrition,
                                                        };
                                                        nutrition[ nutritionField ] = event.target.value;

                                                        this.props.onChangeAddItems([
                                                            {
                                                                ...item,
                                                                nutrition,
                                                            }
                                                        ])
                                                    }}
                                                /> { unit }
                                            </div>
                                            {
                                                label
                                                ?
                                                <div className="wprmprc-collection-action-add-ingredient-nutrient-label">{ label }</div>
                                                :
                                                null
                                            }
                                        </div>
                                    );
                                })
                            }
                        </Fragment>
                    }
                    <label>{ __wprm( 'Color' ) }</label>
                    <Select
                        className="wprmprc-collection-action-add-item-color"
                        value={colorOptions.filter(({value}) => value === item.color)}
                        onChange={(option) =>
                            this.props.onChangeAddItems([
                                {
                                    ...item,
                                    color: option.value,
                                }
                            ])
                        }
                        options={colorOptions}
                        clearable={false}
                        styles={{
                            control: styles => ({ ...styles, borderRadius: 5 }),
                        }}
                    />
                </div>
            </div>
        );
    }
}

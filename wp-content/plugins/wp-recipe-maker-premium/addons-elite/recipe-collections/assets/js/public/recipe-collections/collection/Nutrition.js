import React, { Component, Fragment } from 'react';
import  { formatQuantity } from '../../../../../../../assets/js/shared/quantities';

import { __wprm } from 'Shared/Translations';

import '../../../../css/public/nutrition.scss';
import Api from '../general/Api';
import Loader from '../general/Loader';

export default class Nutrition extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loadingNutrition: true,
            nutrition: [],
        }
    }

    componentDidMount() {
        this.checkRecipes();
    }

    componentDidUpdate( prevProps ) {
        if ( JSON.stringify(this.props.items) !== JSON.stringify(prevProps.items) ) {
            this.checkRecipes();
        }
    }

    checkRecipes() {
        let allRecipes = [];
        let allRecipesServings = [];
        let customRecipes = [];
        let recipesWithoutNutrition = [];

        for ( let item of this.props.items ) {
            if ( 'recipe' === item.type ) {
                let recipe = this.props.recipes.hasOwnProperty(item.recipeId) ? this.props.recipes[item.recipeId] : false;

                if ( ! recipe || ! recipe.hasOwnProperty('nutrition') && ! recipesWithoutNutrition.includes( item.recipeId ) ) {
                    recipesWithoutNutrition.push(item.recipeId);
                }

                let servings = item.hasOwnProperty( 'servings' ) ? item.servings : 1;

                allRecipes.push(item.recipeId);
                allRecipesServings.push( servings );
            } else if ( 'ingredient' === item.type || 'nutrition-ingredient' === item.type ) {
                if ( item.hasOwnProperty( 'nutrition' ) && Object.keys( item.nutrition ).length > 0 ) {
                    customRecipes.push(item.id);
                }
            }
        }

        if ( 0 < recipesWithoutNutrition.length ) {
            this.setState({
                loadingNutrition: true,
            }, () => {
                Api.getNutrition(recipesWithoutNutrition).then((recipes) => {
                    if ( recipes ) {
                        this.props.onUpdateRecipes( recipes );
                        this.updateNutrition( allRecipes, allRecipesServings, customRecipes );
                    }
                });
            });
        } else {
            this.updateNutrition( allRecipes, allRecipesServings, customRecipes );
        }
    }

    updateNutrition(recipes, recipesServings, custom) {
        let nutritionFields = {};

        for ( let nutritionField of wprmprc_public.settings.recipe_collections_nutrition_facts_fields ) {
            // Default to 0.
            if ( ! nutritionFields.hasOwnProperty( nutritionField ) ) {
                nutritionFields[ nutritionField ] = 0.0;
            }

            // Add values for all recipes.
            for ( let i = 0; i < recipes.length; i++ ) {
                const recipeId = recipes[i];
                const recipe = this.props.recipes.hasOwnProperty( recipeId ) ? this.props.recipes[recipeId] : {};
                const recipeNutrition = recipe.hasOwnProperty( 'nutrition' ) ? recipe['nutrition'] : {};

                if ( recipeNutrition.hasOwnProperty( nutritionField ) && recipeNutrition[ nutritionField ] ) {
                    let valueToAdd = recipeNutrition[ nutritionField ];

                    if ( 'total' === wprmprc_public.settings.recipe_collections_nutrition_facts_count ) {
                        if ( recipesServings.hasOwnProperty(i) && 0 <= recipesServings[i] ) {
                            valueToAdd *= recipesServings[i];
                        }
                    } else if ( recipesServings.hasOwnProperty(i) && 0 === recipesServings[i] ) {
                        valueToAdd = 0;
                    }

                    nutritionFields[ nutritionField ] += valueToAdd;
                }
            }

            // Add values for all custom recipes and ingredients.
            for ( let i = 0; i < custom.length; i++ ) {
                const itemId = custom[i];
                const item = this.props.items.find((item) => itemId === item.id);

                if ( item ) {
                    const itemNutrition = item.hasOwnProperty( 'nutrition' ) ? item['nutrition'] : {};

                    if ( itemNutrition.hasOwnProperty( nutritionField ) && itemNutrition[ nutritionField ] ) {
                        let value = parseFloat( itemNutrition[ nutritionField ] );

                        if ( ! isNaN( value ) ) {
                            // If nutrition ingredient also take amount into account.
                            if ( 'nutrition-ingredient' === item.type ) {
                                const amountNew = parseFloat( item.amount );
                                const amountOriginal = parseFloat( item.amountOriginal );

                                if ( ! isNaN( amountNew ) && 0 < amountOriginal && amountNew !== amountOriginal ) {
                                    value = value * ( amountNew / amountOriginal );
                                } else if ( 0.0 === amountNew ) {
                                    value = 0;
                                }
                            }

                            // Check servings.
                            const servings = item.hasOwnProperty( 'servings' ) ? item.servings : 1;

                            if ( 'total' === wprmprc_public.settings.recipe_collections_nutrition_facts_count ) {
                                if ( 0 <= servings ) {
                                    value *= servings;
                                }
                            } else if ( 0 === servings ) {
                                value = 0;
                            }

                            nutritionFields[ nutritionField ] += value;
                        }
                    }
                }
            }
        }

        // Round total values.
        for ( let nutritionField in nutritionFields ) {
            if ( nutritionFields.hasOwnProperty( nutritionField ) && 0 < nutritionFields[nutritionField] ) {
                nutritionFields[nutritionField] = formatQuantity( nutritionFields[nutritionField], wprmprc_public.settings.recipe_collections_nutrition_facts_round_to_decimals );
            }
        }

        this.setState({
            loadingNutrition: false,
            nutrition: nutritionFields,
        })
    }

    render() {
        if ( ! wprmprc_public.settings.recipe_collections_nutrition_facts || 0 === wprmprc_public.settings.recipe_collections_nutrition_facts_fields.length ) {
            return null;
        }

        return (
            <div className="wprmprc-collection-column-nutrition">
                <div className="wprmprc-collection-column-nutrition-header">{
                    'total' === wprmprc_public.settings.recipe_collections_nutrition_facts_count
                    ?
                    __wprm( 'Nutrition Facts' )
                    :
                    __wprm( 'Nutrition Facts (per serving)' )
                }</div>
                <div className="wprmprc-collection-column-nutrition-fields">
                    {
                        this.state.loadingNutrition
                        ?
                        <Loader />
                        :
                        <Fragment>
                            {
                                Object.keys(this.state.nutrition).map((nutritionField) => {
                                    const label = wprmprc_public.labels.nutrition_fields.hasOwnProperty( nutritionField ) ? wprmprc_public.labels.nutrition_fields[ nutritionField ].label : nutritionField;
                                    const value = this.state.nutrition[ nutritionField ];
                                    const unit = wprmprc_public.labels.nutrition_fields.hasOwnProperty( nutritionField ) ? wprmprc_public.labels.nutrition_fields[ nutritionField ].unit : '';

                                    return (
                                        <div className="wprmprc-collection-column-nutrition-field" key={nutritionField}>
                                            <div className="wprmprc-collection-column-nutrition-field-label">{ label }</div>
                                            <div className="wprmprc-collection-column-nutrition-field-value-container">
                                                <span className="wprmprc-collection-column-nutrition-field-value">{ value }</span>
                                                <span className="wprmprc-collection-column-nutrition-field-unit">{ unit }</span>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </Fragment>
                    }
                </div>
            </div>
        );
    }
}
import React, { Component, Fragment } from 'react';
import { withRouter, Link } from 'react-router-dom';

import Api from '../general/Api';
import Loader from '../general/Loader';
import { __wprm } from 'Shared/Translations';

import Collection from './Collection';
import List from './List';
import Options from './Options';

import '../../../../css/public/shopping-list.scss';

class ShoppingList extends Component {

    constructor(props) {
        super(props);

        let uid = props.uid;
        if ( ! uid ) {
            uid = props.collection && props.collection.hasOwnProperty( 'shoppingList' ) ? props.collection.shoppingList : false;
        }

        this.state = {
            uid,
            shoppingList: false,
            shoppingListOriginal: false,
            loading: false === uid ? false : true,
            saving: false,
            editing: false,
            options: {
                notes: false,
                system: 1,
            },
        }
    }

    componentDidMount() {
        if ( false !== this.state.uid ) {
            this.loadShoppingList( this.state.uid );
        }

        window.onbeforeunload = () => {
            return ! this.changesMade() || __wprm( 'There are unsaved changes. Are you sure you want to leave this page?' );
        }
    }

    componentWillUnmount() {
        window.onbeforeunload = null;
    }

    componentDidUpdate( prevProps, prevState ) {
        let uid = this.props.uid;
        if ( ! uid ) {
            uid = this.props.collection && this.props.collection.hasOwnProperty( 'shoppingList' ) ? this.props.collection.shoppingList : false;
        }

        if ( prevState.uid !== uid ) {
            if ( false === uid ) {
                this.setState({
                    uid: false,
                    shoppingList: false,
                    shoppingListOriginal: false,
                    loading: false,
                });
            } else {
                this.loadShoppingList( uid );
            }
        }
    }

    loadShoppingList( uid ) {
        this.setState({
            uid,
            shoppingList: false,
            shoppingListOriginal: false,
            loading: true,
        }, () => {
            Api.getShoppingList( uid ).then((data) => {
                if ( data ) {
                    this.setState({
                        shoppingList: JSON.parse( JSON.stringify( data ) ),
                        shoppingListOriginal: data,
                        loading: false,
                    })
                } else {
                    // Could not load shopping list. Generate new one.
                    let newCollection = JSON.parse( JSON.stringify( this.props.collection ) );
                    newCollection.shoppingList = false;

                    this.props.onChangeCollection( this.props.type, this.props.collection.id, newCollection );
                }
            });
        });
    }

    changesMade() {
        return JSON.stringify( this.state.shoppingList ) !== JSON.stringify( this.state.shoppingListOriginal );
    }
    
    render() {
        const { type, collection } = this.props;
        const { uid } = this.state;

        return (
            <Fragment>
                <div className="wprmprc-container-header">
                    <span className="wprmprc-header-link"
                        onClick={() => {
                            if ( 'shopping' === type || 'saved' === type ) {
                                this.props.history.push(`/`);
                            } else if ( 'inbox' === type ) {
                                this.props.history.push(`/collection/inbox/`);
                            } else {
                                this.props.history.push(`/collection/${type}/${collection.id}`);
                            }
                        }}
                    >{ collection && collection.name ? collection.name : __wprm( 'Back' ) }</span>
                    <span className="wprmprc-header-link-separator">&gt;</span>
                    { __wprm( 'Shopping List' ) }
                </div>
                {
                    this.state.loading
                    ?
                    <Loader/>
                    :
                    <div className={ `wprmprc-shopping-list${ this.state.editing ? ' wprmprc-shopping-list-editing' : ''}`}>
                        {
                            false === uid
                            ?
                            <Fragment>
                                <Collection
                                    { ...this.props }
                                    shoppingList={uid}
                                />
                                {
                                    wprmprc_public.settings.recipe_collections_shopping_list_options
                                    &&
                                    <Options
                                        options={ this.state.options }
                                        onOptionsChange={ (newOptions) => {
                                            this.setState({
                                                options: {
                                                    ...this.state.options,
                                                    ...newOptions,
                                                },
                                            });
                                        }}
                                    />
                                }
                            </Fragment>
                            :
                            <Fragment>
                                <Collection
                                    collection={ this.state.shoppingList.collection }
                                    type={ false }
                                    onChangeCollection={ false }
                                    shoppingList={uid}
                                />
                                <List
                                    groups={ this.state.shoppingList.groups }
                                    onGroupsChange={ (newGroups) => {
                                        let newShoppingList = JSON.parse( JSON.stringify( this.state.shoppingList ) );
                                        newShoppingList.groups = newGroups;

                                        this.setState({
                                            shoppingList: newShoppingList,
                                        });
                                    } }
                                    editing={ this.state.editing }
                                    onEditingChange={ (editing) => {
                                        this.setState({
                                            editing,
                                        });
                                    }}
                                />
                                <div className="wprmprc-shopping-list-meta">
                                    <Link to={ `/shopping-list/${ uid }` }>{ __wprm( 'Right click and copy this link to allow others to edit this shopping list.' ) }</Link>
                                </div>
                            </Fragment>
                        }
                        <div className="wprmprc-shopping-list-actions">
                            {
                                false === uid
                                ?
                                <div
                                    className={ `wprmprc-shopping-list-action wprmprc-shopping-list-action-generate${ this.state.saving ? ' wprmprc-shopping-list-action-disabled' : '' }` }
                                    onClick={ () => {
                                        if ( ! this.state.saving ) {
                                            let hasRecipes = false;

                                            // Check if there are recipes in the shopping list.
                                            collection.columns.map( (column) => {
                                                let inShoppingList;

                                                if ( column.hasOwnProperty( 'inShoppingList' ) ) {
                                                    inShoppingList = column.inShoppingList;
                                                } else {
                                                    // Default to false, unless there's only 1 column.
                                                    inShoppingList = 1 === collection.columns.length;
                                                }
                                                
                                                if ( inShoppingList ) {
                                                    hasRecipes = true;
                                                }
                                            });

                                            if ( hasRecipes ) {
                                                // Generate shopping list.
                                                this.setState({
                                                    saving: true,
                                                }, () => {
                                                    Api.generateShoppingList( type, collection, this.state.options ).then((uid) => {
                                                        this.setState({
                                                            saving: false,
                                                        }, () => {
                                                            if ( uid ) {
                                                                let newCollection = JSON.parse( JSON.stringify( collection ) );
                                                                newCollection.shoppingList = uid;
        
                                                                this.props.onChangeCollection( type, collection.id, newCollection );
                                                            }
                                                        });
                                                    });
                                                });
                                            } else {
                                                alert( __wprm( 'Make sure to select some recipes for the shopping list first.' ) );
                                            }
                                        }
                                    } }
                                >
                                    {
                                        this.state.saving
                                        ?
                                        <Loader />
                                        :
                                        __wprm( 'Generate Shopping List' )
                                    }
                                </div>
                                :
                                <Fragment>
                                    <div
                                        className={ `wprmprc-shopping-list-action wprmprc-shopping-list-action-save${ ! this.changesMade() ? ' wprmprc-shopping-list-action-disabled' : '' }`}
                                        onClick={ () => {
                                            if ( this.changesMade() && ! this.state.saving ) {
                                                this.setState({
                                                    saving: true,
                                                }, () => {
                                                    const shoppingListToSave = JSON.parse( JSON.stringify( this.state.shoppingList ) );

                                                    Api.saveShoppingList( this.state.uid, shoppingListToSave ).then((saved) => {
                                                        let newState = {
                                                            saving: false,
                                                        };
                                                        
                                                        if ( saved ) {
                                                            newState.shoppingListOriginal = shoppingListToSave;
                                                        } else {
                                                            alert( __wprm( 'The shopping list could not be saved. Try again later.' ) );
                                                        }

                                                        this.setState( newState );
                                                    });
                                                });
                                            }
                                        } }
                                    >
                                        {
                                            this.state.saving
                                            ?
                                            <Loader />
                                            :
                                            __wprm( 'Save Shopping List' )
                                        }
                                    </div>
                                    <div
                                        className={ `wprmprc-shopping-list-action wprmprc-shopping-list-action-print${ this.changesMade() ? ' wprmprc-shopping-list-action-disabled' : '' }` }
                                        onClick={ () => {
                                            if ( this.changesMade() ) {
                                                alert( __wprm( 'Make sure to save the shopping list before printing.' ) );
                                            } else {
                                                const printUrl = window.WPRecipeMaker.print.getUrl( 'shopping-list/' + this.state.uid );
                                                window.open( printUrl,'_blank' );
                                            }
                                        } }
                                    >{ __wprm( 'Print Shopping List' ) }</div>
                                    <div
                                        className={ `wprmprc-shopping-list-action wprmprc-shopping-list-action-regenerate${ this.state.saving ? ' wprmprc-shopping-list-action-disabled' : '' }`}
                                        onClick={ () => {
                                            if ( ! this.state.saving && confirm( __wprm( 'Are you sure you want to generate a new shopping list for this collection? You will only be able to access this shopping list again with the share link.' ) ) ) {
                                                let newCollection = JSON.parse( JSON.stringify( collection ) );
                                                newCollection.shoppingList = false;
                                                this.props.onChangeCollection( type, collection.id, newCollection );
                                            }
                                        } }
                                    >
                                        { __wprm( 'Regenerate Shopping List' ) }
                                    </div>
                                </Fragment>
                            }
                        </div>
                    </div>
                }
            </Fragment>
        );
    }
}

export default withRouter(ShoppingList);
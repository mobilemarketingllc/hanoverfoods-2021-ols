import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';

import { __wprm } from 'Shared/Translations';
import EditList from '../general/EditList';
import AddSavedCollection from './AddSavedCollection';

import '../../../../css/public/overview.scss';

class Overview extends Component {
    render() {
        const viewCollection = (type, collection) => (
            <div
                className={`wprmprc-overview-collection wprmprc-overview-collection-view wprmprc-overview-collection-${type}`}
                onClick={() => {
                    if ( 'inbox' === type ) {
                        this.props.history.push(`/collection/inbox/`);
                    } else {
                        this.props.history.push(`/collection/${type}/${collection.id}`);
                    }
                }}
            >
                <div className="wprmprc-overview-collection-name">{collection.name}</div>
                <div className="wprmprc-overview-collection-items">{collection.nbrItems}</div>
            </div>
        );

        const editCollection = (type, collection) => (
            <div className="wprmprc-overview-collection wprmprc-overview-collection-edit">
                <input
                    type="text"
                    value={collection.name}
                    onChange={(event) => {
                        this.props.onChangeCollection(type, collection.id, { name: event.target.value });
                    }}
                />
                <div className="wprmprc-overview-collection-items">{collection.nbrItems}</div>
            </div>
        );

        const isAdminEditingUser = parseInt( wprmprc_public.user ) !== parseInt( wprmprc_public.collections_user );

        return (
            <Fragment>
                <div className="wprmprc-container-header">{ __wprm( 'Your Collections' ) }{ isAdminEditingUser ? ` (${ __wprm( 'Editing User' ) } #${ wprmprc_public.collections_user })` : ''}</div>
                <div className="wprmprc-overview">
                    <EditList
                        type='collection'
                        onAdd={() => this.props.onAddCollection('user')}
                        onDelete={(id) => this.props.onDeleteCollection('user', id)}
                        onDuplicate={(id, index) => {
                            this.props.onAddCollection( 'user', index );
                        }}
                        onReorder={(newItems, oldIndex, newIndex) => this.props.onReorderCollection('user', oldIndex, newIndex)}
                        header={(editing) => editing ? editCollection('inbox', this.props.collections.inbox) : viewCollection('inbox', this.props.collections.inbox) }
                        items={this.props.collections.user}
                        item={(editing, item) => editing ? editCollection('user', item) : viewCollection('user', item)}
                        labels={{
                            add: __wprm( 'Add Collection' ),
                            edit: __wprm( 'Edit Collections' ),
                        }}
                    />
                    {
                        isAdminEditingUser
                        &&
                        <AddSavedCollection
                            onAddSavedCollection={(collection) => {
                                this.props.onAddCollection( 'saved', false, collection );
                            }}
                        />
                    }
                </div>
            </Fragment>
        );
    }
}

export default withRouter(Overview);
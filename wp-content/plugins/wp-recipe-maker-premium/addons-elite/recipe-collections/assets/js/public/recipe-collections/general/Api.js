const collectionsHelperEndpoint = window.wprmp_public ? wprmp_public.endpoints.collections_helper : '';
const debounceTime = 500;

let needsSaveConfirm = window.hasOwnProperty( 'wprmprc_public' ) && parseInt( wprmprc_public.user ) !== parseInt( wprmprc_public.collections_user );

let searchRequest = '';
let searchRequestTimer = null;
let searchPromise = false;

let saveRequest = false;
let saveRequestTimer = null;

export default {
    getCollections() {
        let collections = false;

        // Use collections in local storage.
        const localCollections = localStorage.getItem( 'wprm-recipe-collection' );

        if ( localCollections ) {
            collections = JSON.parse(localCollections);
        }

        // Logged in user with saved collections.
        const savedCollections = wprmprc_public.collections;
        if ( savedCollections ) {
            collections = savedCollections;
        }

        // Use default collections if there are no saved or local ones.
        if ( ! collections ) {
            collections = wprmprc_public.collections_default;
        }

        // Decouple and return.
        return JSON.parse(JSON.stringify(collections));
    },
    saveCollections(collections) {
        if ( 0 === parseInt( wprmp_public.user ) ) {
            // Not logged in, save in local storage.
            localStorage.setItem( 'wprm-recipe-collection', JSON.stringify( collections ) );
        } else {
            // Confirm save when editing someone else's collection the first time.
            if ( ! needsSaveConfirm || confirm( 'Warning: Are you sure you want to edit the collections of this user? This warning will only show once.' ) ) {
                needsSaveConfirm = false;
                saveRequest = collections;

                clearTimeout(saveRequestTimer);
                saveRequestTimer = setTimeout(() => {
                    this.saveCollectionsDebounced();
                }, debounceTime);
            }
        }

        return collections;
    },
    saveCollectionsDebounced() {
        const collections = saveRequest;
        saveRequest = false;

        if ( collections ) {
            return fetch(`${collectionsHelperEndpoint}/user/${wprmprc_public.collections_user}`, {
                method: 'POST',
                headers: {
                    'X-WP-Nonce': wprm_public.api_nonce,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                credentials: 'same-origin',
                body: JSON.stringify({
                    collections,
                }),
            });
        }
    },
    searchRecipes(search) {
        searchRequest = search;

        clearTimeout(searchRequestTimer);
        searchRequestTimer = setTimeout(() => {
            this.searchRecipesDebounced();
        }, debounceTime);

        if ( searchPromise ) {
            searchPromise(false)
        }

        return new Promise( r => searchPromise = r );
    },
    searchRecipesDebounced() {
        const promise = searchPromise;
        const search = searchRequest;
        searchPromise = false;
        searchRequest = '';

        return fetch(`${collectionsHelperEndpoint}/recipes`, {
            method: 'POST',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                search,
            }),
        }).then(response => {
            return response.json().then(json => {
                let result = response.ok ? json : false;
                promise( result );
            });
        });
    },
    searchIngredients(search) {
        searchRequest = search;

        clearTimeout(searchRequestTimer);
        searchRequestTimer = setTimeout(() => {
            this.searchIngredientsDebounced();
        }, debounceTime);

        if ( searchPromise ) {
            searchPromise(false)
        }

        return new Promise( r => searchPromise = r );
    },
    searchIngredientsDebounced() {
        const promise = searchPromise;
        const search = searchRequest;
        searchPromise = false;
        searchRequest = '';

        return fetch(`${collectionsHelperEndpoint}/nutrition-ingredients`, {
            method: 'POST',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                search,
            }),
        }).then(response => {
            return response.json().then(json => {
                let result = response.ok ? json : false;
                promise( result );
            });
        });
    },
    getRecipe(id) {
        return fetch(`${collectionsHelperEndpoint}/recipe/${id}`, {
            method: 'GET',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
        }).then(response => {
            return response.json().then(json => {
                return response.ok ? json : false;
            });
        });
    },
    getIngredients(recipes, ingredients) {
        return fetch(`${collectionsHelperEndpoint}/ingredients`, {
            method: 'POST',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                recipes,
                ingredients,
            }),
        }).then(response => {
            return response.json().then(json => {
                return response.ok ? json : false;
            });
        });
    },
    getNutrition(recipes) {
        return fetch(`${collectionsHelperEndpoint}/nutrition`, {
            method: 'POST',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                recipes,
            }),
        }).then(response => {
            return response.json().then(json => {
                return response.ok ? json : false;
            });
        });
    },
    saveCollectionToCollections(collectionId) {
        return fetch(`${collectionsHelperEndpoint}/save`, {
            method: 'POST',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                collectionId,
            }),
        }).then(response => {
            return response.ok;
        });
    },
    getShoppingList(uid) {
        return fetch(`${collectionsHelperEndpoint}/shopping-list/${uid}`, {
            method: 'GET',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
        }).then(response => {
            return response.json().then(json => {
                return response.ok ? json : false;
            });
        });
    },
    saveShoppingList( uid, data ) {
        return fetch(`${collectionsHelperEndpoint}/shopping-list/${uid}`, {
            method: 'POST',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                data,
            }),
        }).then(response => {
            return response.json().then(json => {
                return response.ok ? json : false;
            });
        });
    },
    generateShoppingList( type, collection, options ) {
        return fetch(`${collectionsHelperEndpoint}/shopping-list`, {
            method: 'POST',
            headers: {
                'X-WP-Nonce': wprm_public.api_nonce,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                type,
                collection,
                options,
            }),
        }).then(response => {
            return response.json().then(json => {
                return response.ok ? json : false;
            });
        });
    },
};

import './public/checkboxes';
import './public/prevent-sleep';
import './public/servings-changer';
import './public/timer';
import './public/user-rating';

import '../css/public/nutrition-label.scss';
import '../css/shortcodes/shortcodes.scss';
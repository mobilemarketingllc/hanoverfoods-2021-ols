const collectionsEndpoint = wprmp_admin.endpoints.collections;

import ApiWrapper from 'Shared/ApiWrapper';

export default {
    delete(id) {
        return ApiWrapper.call( `${collectionsEndpoint}/${id}`, 'DELETE' );
    },
    update(id, collection) {
        const data = {
            collection,
        }
        return ApiWrapper.call( `${collectionsEndpoint}/${id}`, 'POST', data );
    },
};

import React, { Fragment } from 'react';
import he from 'he';
 
import TextFilter from 'Manage/general/TextFilter';
import Api from 'Shared/Api';
import Icon from 'Shared/Icon';
import { __wprm } from 'Shared/Translations';

export default {
    getColumns( datatable ) {
        let columns = [{
            Header: __wprm( 'Sort:' ),
            id: 'actions',
            headerClassName: 'wprm-admin-table-help-text',
            sortable: false,
            width: 100,
            Filter: () => (
                <div>
                    { __wprm( 'Filter:' ) }
                </div>
            ),
            Cell: row => (
                <div className="wprm-admin-manage-actions">
                    <Icon
                        type="pencil"
                        title={ __wprm( 'Edit Saved Collection' ) }
                        onClick={() => {
                            const url = `${wprmp_admin.manage.collections_url}&id=${row.original.id}`;
                            window.location = url;
                        }}
                    />
                    <Icon
                        type="duplicate"
                        title={ __wprm( 'Duplicate Saved Collection' ) }
                        onClick={() => {
                            const url = `${wprmp_admin.manage.collections_url}&action=duplicate&id=${row.original.id}`;
                            window.location = url;
                        }}
                    />
                    <Icon
                        type="trash"
                        title={ __wprm( 'Delete Saved Collection' ) }
                        onClick={() => {
                            if( confirm( `${ __wprm( 'Are you sure you want to delete' ) } "${row.original.name}"?` ) ) {
                                Api.collection.delete(row.original.id).then(() => datatable.refreshData());
                            }
                        }}
                    />
                </div>
            ),
        },{
            Header: __wprm( 'ID' ),
            id: 'id',
            accessor: 'id',
            width: 65,
            Filter: (props) => (<TextFilter {...props}/>),
        },{
            Header: __wprm( 'Date' ),
            id: 'date',
            accessor: 'date',
            width: 150,
            Filter: (props) => (<TextFilter {...props}/>),
        },{
            Header: __wprm( 'Name' ),
            id: 'name',
            accessor: 'name',
            Filter: (props) => (<TextFilter {...props}/>),
            Cell: row => row.value ? he.decode(row.value) : null,
        },{
            Header: __wprm( 'Default' ),
            id: 'default',
            accessor: 'default',
            filterable: false,
            sortable: false,
            width: 65,
            Cell: (row) => (
                <input
                    type="checkbox"
                    checked={ true === row.value }
                    onChange={ (e) => {
                        Api.collection.update(row.original.id, {
                            ...row.original,
                            default: e.target.checked,
                        }).then(() => datatable.refreshData());
                    } }
                />
            ),
        },{
            Header: __wprm( 'Save Collection Link' ),
            id: 'saveLink',
            accessor: 'saveLink',
            filterable: false,
            sortable: false,
            Cell: row => row.value ? <input type="type" value={ row.value } style={ { width: '100%', fontSize: '0.9em', padding: 3, border: 'none' } } readOnly /> : null,
        },{
            Header: __wprm( '# Items' ),
            id: 'nbrItems',
            accessor: 'nbrItems',
            width: 65,
            Filter: (props) => (<TextFilter {...props}/>),
        }];

        return columns;
    }
};
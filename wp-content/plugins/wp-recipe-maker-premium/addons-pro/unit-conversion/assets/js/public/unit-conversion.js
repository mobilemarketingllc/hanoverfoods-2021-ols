import '../../css/public/unit-conversion.scss';

window.WPRecipeMaker.conversion = {
    init() {
        // Add event listeners.
		document.addEventListener( 'click', function(e) {
			if ( e.target.matches( '.wprm-unit-conversion' ) ) {
                e.preventDefault();
				WPRecipeMaker.conversion.clickSystem( e.target );
			}
        }, false );
		document.addEventListener( 'change', function(e) {
			if ( e.target.matches( '.wprm-unit-conversion-dropdown' ) ) {
				WPRecipeMaker.conversion.changeDropdown( e.target );
			}
		}, false );
    },
    clickSystem( elem ) {
        const recipeId = elem.dataset.recipe,
            system = parseInt( elem.dataset.system );

        if ( ! elem.classList.contains( 'wprmpuc-active' ) ) {
            const siblings = elem.parentNode.querySelectorAll( '.wprm-unit-conversion' );
            for ( let sibling of siblings ) {
                sibling.classList.remove( 'wprmpuc-active' );
            }
            elem.classList.add( 'wprmpuc-active' );

            // Track unit conversion action for analytics.
            if ( window.WPRecipeMaker.hasOwnProperty( 'analytics' ) ) {
                window.WPRecipeMaker.analytics.registerActionOnce( recipeId, wprm_public.post_id, 'unit-conversion', {
                    type: 'link',
                });
            }

            this.setSystem( recipeId, system );
        }
    },
    changeDropdown( elem ) {
        const recipeId = elem.dataset.recipe,
            system = parseInt( elem.value );

        // Track unit conversion action for analytics.
        if ( window.WPRecipeMaker.hasOwnProperty( 'analytics' ) ) {
            window.WPRecipeMaker.analytics.registerActionOnce( recipeId, wprm_public.post_id, 'unit-conversion', {
                type: 'dropdown',
            });
        }

        this.setSystem( recipeId, system );
    },
    setSystem( recipeId, system ) {
        const recipe = window.WPRecipeMaker.quantities.getRecipe( recipeId );

        if ( recipe ) {
            const currentServings = recipe.servings;

            // Revert recipe back to original servings before converting.
            window.WPRecipeMaker.quantities.setServings( recipeId, recipe.originalServings );

            // Replace quantities with converted ones.
            const ingredientsData = window.hasOwnProperty( 'wprmpuc_recipe_' + recipeId ) ? window['wprmpuc_recipe_' + recipeId].ingredients : false;

            if ( false === ingredientsData ) {
                return;
            }

            const containers = document.querySelectorAll( `#wprm-recipe-container-${ recipeId }, .wprm-recipe-roundup-item-${ recipeId }, .wprm-print-recipe-${ recipeId }, .wprm-recipe-${ recipeId }-ingredients-container` );
            for ( let container of containers ) {
                const ingredients = container.querySelectorAll( '.wprm-recipe-ingredient' );

                for ( let i = 0; i < ingredients.length; i++ ) {
                    const ingredient = ingredients[i];
                    const converted = ingredientsData.hasOwnProperty( i ) && ingredientsData[i].converted ? ingredientsData[i].converted : {};

                    if ( 1 === system || converted.hasOwnProperty( system ) ) {
                        const amount = 1 === system ? ingredientsData[i].amount : converted[system].amount,
                            unit = 1 === system ? ingredientsData[i].unit : converted[system].unit;
            
                        // Only replace if at least amount or unit is set and an actual number.
                        if ( 'NaN' !== amount && ( amount || unit ) ) {
                            const amountContainer = ingredient.querySelector( '.wprm-recipe-ingredient-amount' );
                            if ( amountContainer ) { amountContainer.innerHTML = amount; }

                            const unitContainer = ingredient.querySelector( '.wprm-recipe-ingredient-unit' );
                            if ( unitContainer ) { unitContainer.innerHTML = unit; }
                        }
                    }
                }
            }

            // Update unit conversion changers.
            const unitConversionChangers = document.querySelectorAll( '.wprm-unit-conversion-container-' + recipe.id );

            for ( let unitConversionChanger of unitConversionChangers ) {
                const dropdown = unitConversionChanger.querySelector('.wprm-unit-conversion-dropdown');

                if ( dropdown ) {
                    dropdown.value = system;
                } else {
                    const links = unitConversionChanger.querySelectorAll( '.wprm-unit-conversion' );
                    for ( let link of links ) {
                        const linkSystem = parseInt( link.dataset.system );
                        if ( system === linkSystem ) {
                            link.classList.add( 'wprmpuc-active' );
                        } else {
                            link.classList.remove( 'wprmpuc-active' );
                        }
                    }
                }
            }

            // Force a reinit of the recipe with these new quantities.
            window.WPRecipeMaker.quantities.initRecipe( recipeId );

            // Set system for this new recipe. Used for printing.
            const newRecipe = window.WPRecipeMaker.quantities.getRecipe( recipeId );
            newRecipe.system = system;
            
            // Back to the servings the recipe was set to.
            window.WPRecipeMaker.quantities.setServings( recipeId, currentServings );
        }
    },
}

ready(() => {
	window.WPRecipeMaker.conversion.init();
});

function ready( fn ) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
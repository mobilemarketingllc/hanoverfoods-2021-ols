== WooCommerce FirstData Gateway
Author: skyverge
Tags: woocommerce
Requires PHP: 5.6
Requires at least: 4.4
Tested up to: 5.5.1

Accept credit cards in WooCommerce through the First Data Payeezy (formerly GGe4) or Global Gateway

See https://docs.woocommerce.com/document/firstdata/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-gateway-firstdata' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress

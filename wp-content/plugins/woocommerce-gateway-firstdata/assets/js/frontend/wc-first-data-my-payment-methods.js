/**
 * The My Payment Methods handler.
 *
 * @since 4.7.3
 */

jQuery( document ).ready( ( $ ) => {

	'use strict';

	window.WC_First_Data_My_Payment_Methods_Handler = window.SV_WC_Payment_Methods_Handler_v5_8_1;

	$( document.body ).trigger( 'wc_first_data_my_payment_methods_handler_loaded' );

} );

/**
 * The payment form handler.
 *
 * @since 4.7.3
 */

jQuery( document ).ready( ( $ ) => {

	'use strict';

	window.WC_First_Data_Payeezy_Gateway_Payment_Form_Handler = window.SV_WC_Payment_Form_Handler_v5_8_1;

	$( document.body ).trigger( 'wc_first_data_payeezy_gateway_payment_form_handler_loaded' );

} );

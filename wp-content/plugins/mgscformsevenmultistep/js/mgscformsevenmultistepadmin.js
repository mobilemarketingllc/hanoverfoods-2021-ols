/*
|--------------------------------------------------------------------------
	Contact Form 7 Multi Step Add-ons Admin JS
	Author: MGScoder
	Author URL: https://codecanyon.net/user/mgscoder
|--------------------------------------------------------------------------
*/
document.addEventListener("touchstart", function() {},false);
(function ($) {
	
	"use strict";
	$('input[name=mgscf7multistepsetting]:first').prop( "checked", true );	
	$("input[name='mgscf7multistepsetting']").on("click", function() {		
		var mgscf7multistepsetting = $('input[name=mgscf7multistepsetting]:checked').val();		
		if( mgscf7multistepsetting == 'defaultSettings' ) {
			$("#defaultSettings").css("display", "block");
			$("#displayStepTitle").css("display", "none");
			$("#changeButtonText").css("display", "none");
			$("#dsiplayFormInput").css("display", "none");
			$("#mgscf7themeSettings").css("display", "none");
			$("#cssSettings").css("display", "none");
		}
		else if( mgscf7multistepsetting == 'displayStepTitle' ) {
			$("#displayStepTitle").css("display", "block");
			$("#defaultSettings").css("display", "none");
			$("#changeButtonText").css("display", "none");
			$("#dsiplayFormInput").css("display", "none");
			$("#mgscf7themeSettings").css("display", "none");
			$("#cssSettings").css("display", "none");
		}
		else if( mgscf7multistepsetting == 'changeButtonText' ) {
			$("#changeButtonText").css("display", "block");
			$("#defaultSettings").css("display", "none");
			$("#displayStepTitle").css("display", "none");
			$("#dsiplayFormInput").css("display", "none");
			$("#mgscf7themeSettings").css("display", "none");
			$("#cssSettings").css("display", "none");
		}
		else if( mgscf7multistepsetting == 'dsiplayFormInput' ) {
			$("#dsiplayFormInput").css("display", "block");
			$("#defaultSettings").css("display", "none");
			$("#displayStepTitle").css("display", "none");
			$("#changeButtonText").css("display", "none");
			$("#mgscf7themeSettings").css("display", "none");
			$("#cssSettings").css("display", "none");
		}
		else if( mgscf7multistepsetting == 'mgscf7themeSettings' ) {
			$("#mgscf7themeSettings").css("display", "block");
			$("#cssSettings").css("display", "none");
			$("#defaultSettings").css("display", "none");
			$("#displayStepTitle").css("display", "none");
			$("#changeButtonText").css("display", "none");
			$("#dsiplayFormInput").css("display", "none");
		}
		else if( mgscf7multistepsetting == 'cssSettings' ) {
			$("#cssSettings").css("display", "block");
			$("#defaultSettings").css("display", "none");
			$("#displayStepTitle").css("display", "none");
			$("#changeButtonText").css("display", "none");
			$("#dsiplayFormInput").css("display", "none");
			$("#mgscf7themeSettings").css("display", "none");
		}
		else {
			$("#defaultSettings").css("display", "block");
			$("#displayStepTitle").css("display", "none");
			$("#changeButtonText").css("display", "none");
			$("#dsiplayFormInput").css("display", "none");
			$("#mgscf7themeSettings").css("display", "none");
			$("#cssSettings").css("display", "none");
		}
	});		
  
})(jQuery);

/*
|--------------------------------------------------------------------------
	End
|--------------------------------------------------------------------------
*/
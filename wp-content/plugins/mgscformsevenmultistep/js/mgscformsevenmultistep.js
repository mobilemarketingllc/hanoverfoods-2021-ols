/*
|--------------------------------------------------------------------------
	Contact Form 7 Multi Step Add-ons JS
	Author: MGScoder
	Author URL: https://codecanyon.net/user/mgscoder
|--------------------------------------------------------------------------
*/
document.addEventListener("touchstart", function() {},false);
(function ($) {
	"use strict";
	var reqemtcount = 0;
	
	$(".mgscfsMultiStepNextStep2").on("click", function (event) {
		mgscfsMultiStepGoNextStep(2);		
	});
	
	$(".mgscfsMultiStepPreviousStep1").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(1);
	});
	
	$(".mgscfsMultiStepNextStep3").on("click", function (event) {
		mgscfsMultiStepGoNextStep(3);
	});
	
	$(".mgscfsMultiStepPreviousStep2").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(2);
	});
	
	$(".mgscfsMultiStepNextStep4").on("click", function (event) {
		mgscfsMultiStepGoNextStep(4);
	});
	
	$(".mgscfsMultiStepPreviousStep3").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(3);
	});
	
	$(".mgscfsMultiStepNextStep5").on("click", function (event) {
		mgscfsMultiStepGoNextStep(5);
	});
	
	$(".mgscfsMultiStepPreviousStep4").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(4);
	});
	
	$(".mgscfsMultiStepNextStep6").on("click", function (event) {
		mgscfsMultiStepGoNextStep(6);
	});
	
	$(".mgscfsMultiStepPreviousStep5").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(5);
	});
	
	$(".mgscfsMultiStepNextStep7").on("click", function (event) {
		mgscfsMultiStepGoNextStep(7);
	});
	
	$(".mgscfsMultiStepPreviousStep6").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(6);
	});
	
	$(".mgscfsMultiStepNextStep8").on("click", function (event) {
		mgscfsMultiStepGoNextStep(8);
	});
	
	$(".mgscfsMultiStepPreviousStep7").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(7);
	});
	
	$(".mgscfsMultiStepNextStep9").on("click", function (event) {
		mgscfsMultiStepGoNextStep(9);
	});
	
	$(".mgscfsMultiStepPreviousStep8").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(8);
	});
	
	$(".mgscfsMultiStepNextStep10").on("click", function (event) {
		mgscfsMultiStepGoNextStep(10);
	});
	
	$(".mgscfsMultiStepPreviousStep9").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(9);
	});
	
	$(".mgscfsMultiStepNextStep11").on("click", function (event) {
		mgscfsMultiStepGoNextStep(11);
	});
	
	$(".mgscfsMultiStepPreviousStep10").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(10);
	});
	
	$(".mgscfsMultiStepNextStep12").on("click", function (event) {
		mgscfsMultiStepGoNextStep(12);
	});
	
	$(".mgscfsMultiStepPreviousStep11").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(11);
	});
	
	$(".mgscfsMultiStepNextStep13").on("click", function (event) {
		mgscfsMultiStepGoNextStep(13);
	});
	
	$(".mgscfsMultiStepPreviousStep12").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(12);
	});
	
	$(".mgscfsMultiStepNextStep14").on("click", function (event) {
		mgscfsMultiStepGoNextStep(14);
	});
	
	$(".mgscfsMultiStepPreviousStep13").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(13);
	});
	
	$(".mgscfsMultiStepNextStep15").on("click", function (event) {
		mgscfsMultiStepGoNextStep(15);
	});
	
	$(".mgscfsMultiStepPreviousStep14").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(14);
	});
	
	$(".mgscfsMultiStepNextStep16").on("click", function (event) {
		mgscfsMultiStepGoNextStep(16);
	});
	
	$(".mgscfsMultiStepPreviousStep15").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(15);
	});
	
	$(".mgscfsMultiStepNextStep17").on("click", function (event) {
		mgscfsMultiStepGoNextStep(17);
	});
	
	$(".mgscfsMultiStepPreviousStep16").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(16);
	});
	
	$(".mgscfsMultiStepNextStep18").on("click", function (event) {
		mgscfsMultiStepGoNextStep(18);
	});
	
	$(".mgscfsMultiStepPreviousStep17").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(17);
	});
	
	$(".mgscfsMultiStepNextStep19").on("click", function (event) {
		mgscfsMultiStepGoNextStep(19);
	});
	
	$(".mgscfsMultiStepPreviousStep18").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(18);
	});
	
	$(".mgscfsMultiStepNextStep20").on("click", function (event) {
		mgscfsMultiStepGoNextStep(20);
	});
	
	$(".mgscfsMultiStepPreviousStep19").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(19);
	});
	
	$(".mgscfsMultiStepNextStep21").on("click", function (event) {
		mgscfsMultiStepGoNextStep(21);
	});
	
	$(".mgscfsMultiStepPreviousStep20").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(20);
	});
	
	$(".mgscfsMultiStepNextStep22").on("click", function (event) {
		mgscfsMultiStepGoNextStep(22);
	});
	
	$(".mgscfsMultiStepPreviousStep21").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(21);
	});
	
	$(".mgscfsMultiStepNextStep23").on("click", function (event) {
		mgscfsMultiStepGoNextStep(23);
	});
	
	$(".mgscfsMultiStepPreviousStep22").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(22);
	});
	
	$(".mgscfsMultiStepNextStep24").on("click", function (event) {
		mgscfsMultiStepGoNextStep(24);
	});
	
	$(".mgscfsMultiStepPreviousStep23").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(23);
	});
	
	$(".mgscfsMultiStepNextStep25").on("click", function (event) {
		mgscfsMultiStepGoNextStep(25);
	});
	
	$(".mgscfsMultiStepPreviousStep24").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(24);
	});
	
	$(".mgscfsMultiStepNextStep26").on("click", function (event) {
		mgscfsMultiStepGoNextStep(26);
	});
	
	$(".mgscfsMultiStepPreviousStep25").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(25);
	});
	
	$(".mgscfsMultiStepNextStep27").on("click", function (event) {
		mgscfsMultiStepGoNextStep(27);
	});
	
	$(".mgscfsMultiStepPreviousStep26").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(26);
	});
	
	$(".mgscfsMultiStepNextStep28").on("click", function (event) {
		mgscfsMultiStepGoNextStep(28);
	});
	
	$(".mgscfsMultiStepPreviousStep27").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(27);
	});
	
	$(".mgscfsMultiStepNextStep29").on("click", function (event) {
		mgscfsMultiStepGoNextStep(29);
	});
	
	$(".mgscfsMultiStepPreviousStep28").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(28);
	});
	
	$(".mgscfsMultiStepNextStep30").on("click", function (event) {
		mgscfsMultiStepGoNextStep(30);
	});
	
	$(".mgscfsMultiStepPreviousStep29").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(29);
	});
	
	$(".mgscfsMultiStepNextStep31").on("click", function (event) {
		mgscfsMultiStepGoNextStep(31);
	});
	
	$(".mgscfsMultiStepPreviousStep30").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(30);
	});
	
	$(".mgscfsMultiStepNextStep32").on("click", function (event) {
		mgscfsMultiStepGoNextStep(32);
	});
	
	$(".mgscfsMultiStepPreviousStep31").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(31);
	});
	
	$(".mgscfsMultiStepNextStep33").on("click", function (event) {
		mgscfsMultiStepGoNextStep(33);
	});
	
	$(".mgscfsMultiStepPreviousStep32").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(32);
	});
	
	$(".mgscfsMultiStepNextStep34").on("click", function (event) {
		mgscfsMultiStepGoNextStep(34);
	});
	
	$(".mgscfsMultiStepPreviousStep33").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(33);
	});
	
	$(".mgscfsMultiStepNextStep35").on("click", function (event) {
		mgscfsMultiStepGoNextStep(35);
	});
	
	$(".mgscfsMultiStepPreviousStep34").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(34);
	});
	
	$(".mgscfsMultiStepNextStep36").on("click", function (event) {
		mgscfsMultiStepGoNextStep(36);
	});
	
	$(".mgscfsMultiStepPreviousStep35").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(35);
	});
	
	$(".mgscfsMultiStepNextStep37").on("click", function (event) {
		mgscfsMultiStepGoNextStep(37);
	});
	
	$(".mgscfsMultiStepPreviousStep36").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(36);
	});
	
	$(".mgscfsMultiStepNextStep38").on("click", function (event) {
		mgscfsMultiStepGoNextStep(38);
	});
	
	$(".mgscfsMultiStepPreviousStep37").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(37);
	});
	
	$(".mgscfsMultiStepNextStep39").on("click", function (event) {
		mgscfsMultiStepGoNextStep(39);
	});
	
	$(".mgscfsMultiStepPreviousStep38").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(38);
	});
	
	$(".mgscfsMultiStepNextStep40").on("click", function (event) {
		mgscfsMultiStepGoNextStep(40);
	});
	
	$(".mgscfsMultiStepPreviousStep39").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(39);
	});
	
	$(".mgscfsMultiStepNextStep41").on("click", function (event) {
		mgscfsMultiStepGoNextStep(41);
	});
	
	$(".mgscfsMultiStepPreviousStep40").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(40);
	});
	
	$(".mgscfsMultiStepNextStep42").on("click", function (event) {
		mgscfsMultiStepGoNextStep(42);
	});
	
	$(".mgscfsMultiStepPreviousStep41").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(41);
	});
	
	$(".mgscfsMultiStepNextStep43").on("click", function (event) {
		mgscfsMultiStepGoNextStep(43);
	});
	
	$(".mgscfsMultiStepPreviousStep42").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(42);
	});
	
	$(".mgscfsMultiStepNextStep44").on("click", function (event) {
		mgscfsMultiStepGoNextStep(44);
	});
	
	$(".mgscfsMultiStepPreviousStep43").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(43);
	});
	
	$(".mgscfsMultiStepNextStep45").on("click", function (event) {
		mgscfsMultiStepGoNextStep(45);
	});
	
	$(".mgscfsMultiStepPreviousStep44").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(44);
	});
	
	$(".mgscfsMultiStepNextStep46").on("click", function (event) {
		mgscfsMultiStepGoNextStep(46);
	});
	
	$(".mgscfsMultiStepPreviousStep45").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(45);		
	});
	
	$(".mgscfsMultiStepNextStep47").on("click", function (event) {
		mgscfsMultiStepGoNextStep(47);
	});
	
	$(".mgscfsMultiStepPreviousStep46").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(46);
	});
	
	$(".mgscfsMultiStepNextStep48").on("click", function (event) {
		mgscfsMultiStepGoNextStep(48);
	});
	
	$(".mgscfsMultiStepPreviousStep47").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(47);
	});
	
	$(".mgscfsMultiStepNextStep49").on("click", function (event) {
		mgscfsMultiStepGoNextStep(49);
	});
	
	$(".mgscfsMultiStepPreviousStep48").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(48);
	});
	
	$(".mgscfsMultiStepNextStep50").on("click", function (event) {
		mgscfsMultiStepGoNextStep(50);
	});
	
	$(".mgscfsMultiStepPreviousStep49").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(49);
	});
	
	$(".mgscfsMultiStepNextStep51").on("click", function (event) {
		mgscfsMultiStepGoNextStep(51);
	});
	
	$(".mgscfsMultiStepPreviousStep50").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(50);
	});
	
	$(".mgscfsMultiStepNextStep52").on("click", function (event) {
		mgscfsMultiStepGoNextStep(52);
	});
	
	$(".mgscfsMultiStepPreviousStep51").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(51);
	});
	
	$(".mgscfsMultiStepNextStep53").on("click", function (event) {
		mgscfsMultiStepGoNextStep(53);
	});
	
	$(".mgscfsMultiStepPreviousStep52").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(52);
	});
	
	$(".mgscfsMultiStepNextStep54").on("click", function (event) {
		mgscfsMultiStepGoNextStep(54);
	});
	
	$(".mgscfsMultiStepPreviousStep53").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(53);
	});
	
	$(".mgscfsMultiStepNextStep55").on("click", function (event) {
		mgscfsMultiStepGoNextStep(55);
	});
	
	$(".mgscfsMultiStepPreviousStep54").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(54);
	});
	
	$(".mgscfsMultiStepNextStep56").on("click", function (event) {
		mgscfsMultiStepGoNextStep(56);
	});
	
	$(".mgscfsMultiStepPreviousStep55").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(55);
	});
	
	$(".mgscfsMultiStepNextStep57").on("click", function (event) {
		mgscfsMultiStepGoNextStep(57);
	});
	
	$(".mgscfsMultiStepPreviousStep56").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(56);
	});
	
	$(".mgscfsMultiStepNextStep58").on("click", function (event) {
		mgscfsMultiStepGoNextStep(58);
	});
	
	$(".mgscfsMultiStepPreviousStep57").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(57);
	});
	
	$(".mgscfsMultiStepNextStep59").on("click", function (event) {
		mgscfsMultiStepGoNextStep(59);
	});
	
	$(".mgscfsMultiStepPreviousStep58").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(58);
	});
	
	$(".mgscfsMultiStepNextStep60").on("click", function (event) {
		mgscfsMultiStepGoNextStep(60);
	});
	
	$(".mgscfsMultiStepPreviousStep59").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(59);
	});
	
	$(".mgscfsMultiStepNextStep61").on("click", function (event) {
		mgscfsMultiStepGoNextStep(61);
	});
	
	$(".mgscfsMultiStepPreviousStep60").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(60);
	});
	
	$(".mgscfsMultiStepNextStep62").on("click", function (event) {
		mgscfsMultiStepGoNextStep(62);
	});
	
	$(".mgscfsMultiStepPreviousStep61").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(61);
	});
	
	$(".mgscfsMultiStepNextStep63").on("click", function (event) {
		mgscfsMultiStepGoNextStep(63);
	});
	
	$(".mgscfsMultiStepPreviousStep62").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(62);
	});
	
	$(".mgscfsMultiStepNextStep64").on("click", function (event) {
		mgscfsMultiStepGoNextStep(64);
	});
	
	$(".mgscfsMultiStepPreviousStep63").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(63);
	});
	
	$(".mgscfsMultiStepNextStep65").on("click", function (event) {
		mgscfsMultiStepGoNextStep(65);
	});
	
	$(".mgscfsMultiStepPreviousStep64").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(64);
	});
	
	$(".mgscfsMultiStepNextStep66").on("click", function (event) {
		mgscfsMultiStepGoNextStep(66);
	});
	
	$(".mgscfsMultiStepPreviousStep65").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(65);
	});
	
	$(".mgscfsMultiStepNextStep67").on("click", function (event) {
		mgscfsMultiStepGoNextStep(67);
	});
	
	$(".mgscfsMultiStepPreviousStep66").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(66);
	});
	
	$(".mgscfsMultiStepNextStep68").on("click", function (event) {
		mgscfsMultiStepGoNextStep(68);
	});
	
	$(".mgscfsMultiStepPreviousStep67").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(67);
	});
	
	$(".mgscfsMultiStepNextStep69").on("click", function (event) {
		mgscfsMultiStepGoNextStep(69);
	});
	
	$(".mgscfsMultiStepPreviousStep68").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(68);
	});
	
	$(".mgscfsMultiStepNextStep70").on("click", function (event) {
		mgscfsMultiStepGoNextStep(70);
	});
	
	$(".mgscfsMultiStepPreviousStep69").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(69);
	});
	
	$(".mgscfsMultiStepNextStep71").on("click", function (event) {
		mgscfsMultiStepGoNextStep(71);
	});
	
	$(".mgscfsMultiStepPreviousStep70").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(70);
	});
	
	$(".mgscfsMultiStepNextStep72").on("click", function (event) {
		mgscfsMultiStepGoNextStep(72);
	});
	
	$(".mgscfsMultiStepPreviousStep71").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(71);
	});
	
	$(".mgscfsMultiStepNextStep73").on("click", function (event) {
		mgscfsMultiStepGoNextStep(73);
	});
	
	$(".mgscfsMultiStepPreviousStep72").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(72);
	});
	
	$(".mgscfsMultiStepNextStep74").on("click", function (event) {
		mgscfsMultiStepGoNextStep(74);
	});
	
	$(".mgscfsMultiStepPreviousStep73").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(73);
	});
	
	$(".mgscfsMultiStepNextStep75").on("click", function (event) {
		mgscfsMultiStepGoNextStep(75);
	});
	
	$(".mgscfsMultiStepPreviousStep74").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(74);
	});
	
	$(".mgscfsMultiStepNextStep76").on("click", function (event) {
		mgscfsMultiStepGoNextStep(76);
	});
	
	$(".mgscfsMultiStepPreviousStep75").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(75);
	});
	
	$(".mgscfsMultiStepNextStep77").on("click", function (event) {
		mgscfsMultiStepGoNextStep(77);
	});
	
	$(".mgscfsMultiStepPreviousStep76").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(76);
	});
	
	$(".mgscfsMultiStepNextStep78").on("click", function (event) {
		mgscfsMultiStepGoNextStep(78);
	});
	
	$(".mgscfsMultiStepPreviousStep77").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(77);
	});
	
	$(".mgscfsMultiStepNextStep79").on("click", function (event) {
		mgscfsMultiStepGoNextStep(79);
	});
	
	$(".mgscfsMultiStepPreviousStep78").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(78);
	});
	
	$(".mgscfsMultiStepNextStep80").on("click", function (event) {
		mgscfsMultiStepGoNextStep(80);
	});
	
	$(".mgscfsMultiStepPreviousStep79").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(79);
	});
	
	$(".mgscfsMultiStepNextStep81").on("click", function (event) {
		mgscfsMultiStepGoNextStep(81);
	});
	
	$(".mgscfsMultiStepPreviousStep80").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(80);
	});
	
	$(".mgscfsMultiStepNextStep82").on("click", function (event) {
		mgscfsMultiStepGoNextStep(82);
	});
	
	$(".mgscfsMultiStepPreviousStep81").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(81);
	});
	
	$(".mgscfsMultiStepNextStep83").on("click", function (event) {
		mgscfsMultiStepGoNextStep(83);
	});
	
	$(".mgscfsMultiStepPreviousStep82").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(82);
	});
	
	$(".mgscfsMultiStepNextStep84").on("click", function (event) {
		mgscfsMultiStepGoNextStep(84);
	});
	
	$(".mgscfsMultiStepPreviousStep83").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(83);
	});
	
	$(".mgscfsMultiStepNextStep85").on("click", function (event) {
		mgscfsMultiStepGoNextStep(85);
	});
	
	$(".mgscfsMultiStepPreviousStep84").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(84);
	});
	
	$(".mgscfsMultiStepNextStep86").on("click", function (event) {
		mgscfsMultiStepGoNextStep(86);
	});
	
	$(".mgscfsMultiStepPreviousStep85").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(85);
	});
	
	$(".mgscfsMultiStepNextStep87").on("click", function (event) {
		mgscfsMultiStepGoNextStep(87);
	});
	
	$(".mgscfsMultiStepPreviousStep86").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(86);
	});
	
	$(".mgscfsMultiStepNextStep88").on("click", function (event) {
		mgscfsMultiStepGoNextStep(88);
	});
	
	$(".mgscfsMultiStepPreviousStep87").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(87);
	});
	
	$(".mgscfsMultiStepNextStep89").on("click", function (event) {
		mgscfsMultiStepGoNextStep(89);
	});
	
	$(".mgscfsMultiStepPreviousStep88").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(88);
	});
	
	$(".mgscfsMultiStepNextStep90").on("click", function (event) {
		mgscfsMultiStepGoNextStep(90);
	});
	
	$(".mgscfsMultiStepPreviousStep89").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(89);
	});
	
	$(".mgscfsMultiStepNextStep91").on("click", function (event) {
		mgscfsMultiStepGoNextStep(91);
	});
	
	$(".mgscfsMultiStepPreviousStep90").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(90);
	});
	
	$(".mgscfsMultiStepNextStep92").on("click", function (event) {
		mgscfsMultiStepGoNextStep(92);
	});
	
	$(".mgscfsMultiStepPreviousStep91").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(91);
	});
	
	$(".mgscfsMultiStepNextStep93").on("click", function (event) {
		mgscfsMultiStepGoNextStep(93);
	});
	
	$(".mgscfsMultiStepPreviousStep92").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(92);
	});
	
	$(".mgscfsMultiStepNextStep94").on("click", function (event) {
		mgscfsMultiStepGoNextStep(94);
	});
	
	$(".mgscfsMultiStepPreviousStep93").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(93);
	});
	
	$(".mgscfsMultiStepNextStep95").on("click", function (event) {
		mgscfsMultiStepGoNextStep(95);
	});
	
	$(".mgscfsMultiStepPreviousStep94").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(94);
	});
	
	$(".mgscfsMultiStepNextStep96").on("click", function (event) {
		mgscfsMultiStepGoNextStep(96);
	});
	
	$(".mgscfsMultiStepPreviousStep95").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(95);
	});
	
	$(".mgscfsMultiStepNextStep97").on("click", function (event) {
		mgscfsMultiStepGoNextStep(97);
	});
	
	$(".mgscfsMultiStepPreviousStep96").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(96);
	});
	
	$(".mgscfsMultiStepNextStep98").on("click", function (event) {
		mgscfsMultiStepGoNextStep(98);
	});
	
	$(".mgscfsMultiStepPreviousStep97").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(97);
	});
	
	$(".mgscfsMultiStepNextStep99").on("click", function (event) {
		mgscfsMultiStepGoNextStep(99);
	});
	
	$(".mgscfsMultiStepPreviousStep98").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(98);
	});
	
	$(".mgscfsMultiStepNextStep100").on("click", function (event) {
		mgscfsMultiStepGoNextStep(100);
	});
	
	$(".mgscfsMultiStepPreviousStep99").on("click", function (event) {
		mgscfsMultiStepGoPreviousStep(99);
	});
	
/*
|--------------------------------------------------------------------------
	Common
|--------------------------------------------------------------------------
*/		
	function mgscfsMultiStepGoNextStep(nextstepno){
		
		var nextstepno = parseInt(nextstepno);
		var nextstepnext1 = parseInt(nextstepno) + parseInt(1);
		var nextstepnext2 = parseInt(nextstepno) + parseInt(2);
		var nextstepprevious1 = parseInt(nextstepno) - parseInt(1);
		var nextstepprevious2 = parseInt(nextstepno) - parseInt(2);
				
		var reqemailval = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-validates-as-email').val();
		var mgscfsMultiStepEmailError = '';
		var validemail = 1;
		if(reqemailval){
			var validemail = isEmail(reqemailval);
			if (!validemail)
				var mgscfsMultiStepEmailError = mgscfsmultistep_translate_string.email_invalid;
		}
		
		var reqtelval = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-validates-as-tel').val();
		var mgscfsMultiStepTelError = '';
		var validtel = 1;
		if(reqtelval){
			var validtel = isPhone(reqtelval);
			if (!validtel)
				var mgscfsMultiStepTelError = mgscfsmultistep_translate_string.tel_invalid;
		}
		
		var requrlval = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-validates-as-url').val();
		var mgscfsMultiStepUrlError = '';
		var validurl = 1;
		if(requrlval){
			var validurl = isUrlValid(requrlval);
			if (!validurl)
				var mgscfsMultiStepUrlError = mgscfsmultistep_translate_string.url_invalid;
		}
		
		var reqhidfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 + " div.wpcf7cf-hidden" ).find('.wpcf7-validates-as-required').not('.wpcf7-file');
		conditionalHiddenFieldManage(reqhidfield);
		
		var reqattfilehfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 + " div.wpcf7cf-hidden" ).find('.wpcf7-file.wpcf7-validates-as-required');
		conditionalHiddenFileFieldManage(reqattfilehfield);
		
		var reqfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-validates-as-required').not('.wpcf7-checkbox');
		reqemtcount = 0;
		reqfield.each(function(idx, elem) {
            $(elem).removeClass("mgswpcf7fieldMandatory"); reqemtcount = 0;
			$(elem).closest('span').find('.wpcf7-not-valid-tip').remove();
            if($(elem).val().trim() === "") {
                $(elem).addClass("mgswpcf7fieldMandatory"); reqemtcount = 1;
				$(elem).after('<span role="alert" class="wpcf7-not-valid-tip">'+ mgscfsmultistep_translate_string.field_required +'</span>');
            }
		});
		
		var reqemailfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-validates-as-email');
		var reqemtcountemail = 0;
		reqemailfield.each(function(eidx, eelem) {
            $(eelem).removeClass("mgswpcf7fieldMandatory"); reqemtcountemail = 0;
			if($(eelem).val()) {
				$(eelem).closest('span').find('.wpcf7-not-valid-tip').remove(); reqemtcountemail = 0;
			}else{
                $(eelem).addClass("mgswpcf7fieldMandatory"); reqemtcountemail = 1;
			}	
			
            if($(eelem).val() && !isEmail($(eelem).val())) {
                $(eelem).addClass("mgswpcf7fieldMandatory"); reqemtcountemail = 1;
				$(eelem).after('<span role="alert" class="wpcf7-not-valid-tip">'+ mgscfsmultistep_translate_string.email_invalid +'</span>');
            }
		});
		
		var reqtelfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-validates-as-tel');
		var reqemtcounttel = 0;
		reqtelfield.each(function(tidx, telem) {
            $(telem).removeClass("mgswpcf7fieldMandatory"); reqemtcounttel = 0;
			if($(telem).val()) {
				$(telem).closest('span').find('.wpcf7-not-valid-tip').remove(); reqemtcounttel = 0;
			}else{
                $(telem).addClass("mgswpcf7fieldMandatory"); reqemtcounttel = 1;
			}	
			
            if($(telem).val() && !isPhone($(telem).val())) {
                $(telem).addClass("mgswpcf7fieldMandatory"); reqemtcounttel = 1;
				$(telem).after('<span role="alert" class="wpcf7-not-valid-tip">'+ mgscfsmultistep_translate_string.tel_invalid +'</span>');
            }
		});
		
		var requrlfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-validates-as-url');
		var reqemtcounturl = 0;
		requrlfield.each(function(uidx, uelem) {
            $(uelem).removeClass("mgswpcf7fieldMandatory"); reqemtcounturl = 0;
			if($(uelem).val()) {
				$(uelem).closest('span').find('.wpcf7-not-valid-tip').remove(); reqemtcounturl = 0;
			}else{
                $(uelem).addClass("mgswpcf7fieldMandatory"); reqemtcounturl = 1;
			}	
			
            if($(uelem).val() && !isUrlValid($(uelem).val())) {
                $(uelem).addClass("mgswpcf7fieldMandatory"); reqemtcounturl = 1;
				$(uelem).after('<span role="alert" class="wpcf7-not-valid-tip">'+ mgscfsmultistep_translate_string.url_invalid +'</span>');
            }
		});
		
		var reqattfilefield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-file.wpcf7-validates-as-required');
		var reqemtfilecount = 0;
		reqattfilefield.each(function(fidx, felem) {
            $(felem).removeClass("mgswpcf7fieldMandatory"); reqemtfilecount = 0;
            if(!$(felem).val()) {
                $(felem).addClass("mgswpcf7fieldMandatory"); reqemtfilecount = 1;
            }
		});
		
		for( var i = reqfield.length; i > reqemtcount; i-- ) {
			if( reqfield[i - 1].value == '' ) {
				reqemtcount = reqemtcount + 1;
			}
		}
		
		var reqchkfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 ).find('.wpcf7-checkbox.wpcf7-validates-as-required');
		var reqemtchkcount = 0;
		reqchkfield.each(function(chkidx, chkelem) {
            $(chkelem).removeClass("mgswpcf7fieldMandatory"); reqemtchkcount = reqemtchkcount + 0;
			$(chkelem).parent().find('span.wpcf7-not-valid-tip').remove();
			var fldname = $(chkelem).find("input").attr('name');
			var fldnamev = $('input[name="'+fldname+'"]:checked').val();
			if(!fldnamev) {
                $(chkelem).addClass("mgswpcf7fieldMandatory"); reqemtchkcount = reqemtchkcount + 1;
				$(chkelem).after('<span role="alert" class="wpcf7-not-valid-tip">'+ mgscfsmultistep_translate_string.field_required +'</span>');
            }
		});
		
		var reqhidfield = $( "#mgswpcf7multistepsection-" + nextstepprevious1 + " div.wpcf7cf-hidden" ).find('.wpcf7-validates-as-required');
		conditionalHiddenFieldrefresh(reqhidfield);
		
		if (reqemtcount == 0 && reqemtcountemail == 0 && reqemtcounttel == 0 && reqemtcounturl == 0 && validemail && validtel && validurl && reqemtfilecount == 0 && reqemtchkcount == 0) {
			$( "#mgswpcf7multistepsection-" + nextstepprevious1 + " .help-block.with-errors.mandatory-error" ).html( '' );
			$( "#mgswpcf7multistepsection-" + nextstepprevious1 ).removeClass( "open" );
			$( "#mgswpcf7multistepsection-" + nextstepprevious1 ).addClass( "slide-left" );
			$( "#mgswpcf7multistepsection-" + nextstepno ).removeClass( "slide-right" );
			$( "#mgswpcf7multistepsection-" + nextstepno ).addClass( "open" );
			if(nextstepno == 2){
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepprevious1 ).removeClass( "open" );
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepprevious1 ).addClass( "slide-left" );
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepnext1 ).removeClass( "slide-right" );
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepnext1 ).addClass( "open" );
			}
			else{
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepno ).removeClass( "open" );
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepno ).addClass( "slide-left" );
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepnext1 ).removeClass( "slide-right" );
				$( ".wpcf7-form .mgswpcf7multistep-btn-box-" + nextstepnext1 ).addClass( "open" );
			}
			
			$([document.documentElement, document.body]).animate({
				scrollTop: $("#mgswpcf7multistepsection-" + nextstepno).offset().top - 120
			}, 1000);
			
		}
		else{
			$( "#mgswpcf7multistepsection-" + nextstepprevious1 + " .help-block.with-errors.mandatory-error" ).html( mgscfsMultiStepEmailError + mgscfsMultiStepTelError + mgscfsMultiStepUrlError + mgscfsmultistep_translate_string.fields_error );
		}
		
	}
	
	function mgscfsMultiStepGoPreviousStep(previousstepno){
		
		var previousstepno = parseInt(previousstepno);
		var currentstepno = parseInt(previousstepno) + parseInt(1);
		var currentstepnextstep = parseInt(previousstepno) + parseInt(2);
				
		$( "#mgswpcf7multistepsection-"+previousstepno ).removeClass( "slide-left" );
		$( "#mgswpcf7multistepsection-"+previousstepno ).addClass( "open" );
		$( "#mgswpcf7multistepsection-"+currentstepno ).removeClass( "open" );
		$( "#mgswpcf7multistepsection-"+currentstepno ).addClass( "slide-right" );
		
		$( ".wpcf7-form .mgswpcf7multistep-btn-box-"+currentstepnextstep ).removeClass( "open" );
		$( ".wpcf7-form .mgswpcf7multistep-btn-box-"+currentstepnextstep ).addClass( "slide-right" );
		if(previousstepno == 1){
			$( ".wpcf7-form .mgswpcf7multistep-btn-box-"+previousstepno ).removeClass( "slide-left" );
			$( ".wpcf7-form .mgswpcf7multistep-btn-box-"+previousstepno ).addClass( "open" );
		}
		else{
			$( ".wpcf7-form .mgswpcf7multistep-btn-box-"+currentstepno ).removeClass( "slide-left" );
			$( ".wpcf7-form .mgswpcf7multistep-btn-box-"+currentstepno ).addClass( "open" );
		}
		
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#mgswpcf7multistepsection-" + currentstepno).offset().top - 120
		}, 1000);
				
		if($('.toggle-get-mgswpcf7multistep-btn').find('.fa-arrow-alt-circle-down')){
			$('.toggle-get-mgswpcf7multistep-btn').find('.far').addClass('fa-arrow-alt-circle-down').removeClass('fa-arrow-alt-circle-up');
			$('.mgswpcf7multistep-confirmation-box').hide('slow');
		}	
				
	}
	
	$('.toggle-get-mgswpcf7multistep-btn').on("click", function(e) {
		mgswpcf7multistepConfirmField();
		$('.mgswpcf7multistep-confirmation-box').slideToggle('slow');
		$('.toggle-get-mgswpcf7multistep-btn').find('.far').toggleClass('fa-arrow-alt-circle-down fa-arrow-alt-circle-up');
		return false;
	});	
	
	$('.mgswpcf7btn-custom.mgswpcf7msgoconfirmstep').on("click", function(e) {
		mgswpcf7multistepConfirmField();
		$('.mgswpcf7multistep-confirmation-box').show('slow');
		$('.toggle-get-mgswpcf7multistep-btn').find('.far').addClass('fa-arrow-alt-circle-up').removeClass('fa-arrow-alt-circle-down');
		return false;
	});
	
	function mgswpcf7multistepConfirmField() {
		var mgswpcf7msteplnfnvaluestr = $('input#mgswpcf7msteplnfnvaluestr').val();
		var mgswpcf7msteplnfnamestr = mgswpcf7msteplnfnvaluestr.slice(0,-1);
		var mgswpcf7msteplnfnamestrarray = mgswpcf7msteplnfnamestr.split('|');
		
		var i;
		for (i = 0; i < mgswpcf7msteplnfnamestrarray.length; ++i) {
			
			if(jQuery('input[name='+mgswpcf7msteplnfnamestrarray[i]+'][type=checkbox]:checked').val()){
				jQuery( "#"+mgswpcf7msteplnfnamestrarray[i] ).html( jQuery('input[name='+mgswpcf7msteplnfnamestrarray[i]+'][type=checkbox]:checked').val() );
			}
			else if(jQuery('input[name="'+mgswpcf7msteplnfnamestrarray[i]+'[]"][type=checkbox]:checked').val()){
				var multichkitem = '';
				jQuery('input[name="'+mgswpcf7msteplnfnamestrarray[i]+'[]"][type=checkbox]:checked').each(function(){        
					var multichkivalues = $(this).val().split('|')[0];
					multichkitem += multichkivalues + ', ';
				});
				var multichkitems = multichkitem.slice(0,-2);
				jQuery( "#"+mgswpcf7msteplnfnamestrarray[i] ).html( multichkitems );
			}
			else if(jQuery('input[name='+mgswpcf7msteplnfnamestrarray[i]+']:checked').val()){
				jQuery( "#"+mgswpcf7msteplnfnamestrarray[i] ).html( jQuery('input[name='+mgswpcf7msteplnfnamestrarray[i]+']:checked').val() );
			}
			else if(jQuery('input[name='+mgswpcf7msteplnfnamestrarray[i]+']').val()){
				jQuery( "#"+mgswpcf7msteplnfnamestrarray[i] ).html( jQuery('input[name='+mgswpcf7msteplnfnamestrarray[i]+']').val() );
			}
			else if(jQuery('select[name='+mgswpcf7msteplnfnamestrarray[i]+']').val()){
				jQuery( "#"+mgswpcf7msteplnfnamestrarray[i] ).html( jQuery('select[name='+mgswpcf7msteplnfnamestrarray[i]+']').val() );
			}
			else if(jQuery('textarea[name='+mgswpcf7msteplnfnamestrarray[i]+']').val()){
				jQuery( "#"+mgswpcf7msteplnfnamestrarray[i] ).html( jQuery('textarea[name='+mgswpcf7msteplnfnamestrarray[i]+']').val() );
			}
			else{
				jQuery( "#"+mgswpcf7msteplnfnamestrarray[i] ).html( jQuery('input[name='+mgswpcf7msteplnfnamestrarray[i]+']').val() );
			}
			
		}
	}
	
	function conditionalHiddenFieldManage(reqhidfield) {
		reqhidfield.each(function(chkhfidx, chkhfelem) {
            var hffldname = $(chkhfelem).attr('name');
			if( !$('input[name="'+hffldname+'"]').val() ){
				if( $('input[name="'+hffldname+'"]').hasClass('wpcf7-validates-as-email') )
					$('input[name="'+hffldname+'"]').val("example@example.com");
				else if( $('input[name="'+hffldname+'"]').hasClass('wpcf7-validates-as-tel') )
					$('input[name="'+hffldname+'"]').val("1111111");
				else if( $('input[name="'+hffldname+'"]').hasClass('wpcf7-validates-as-url') )
					$('input[name="'+hffldname+'"]').val("http://www.example.com");
				else if( $('input[name="'+hffldname+'"]').hasClass('wpcf7-number') )
					$('input[name="'+hffldname+'"]').val("1");
				else if( $('textarea[name="'+hffldname+'"]').hasClass('wpcf7-textarea') )
					$('textarea[name="'+hffldname+'"]').val("Dummytext");
				else if( $('input[name="'+hffldname+'"]').hasClass('wpcf7-date') ){
					var now = new Date(); 
					var day = ("0" + now.getDate()).slice(-2);
					var month = ("0" + (now.getMonth() + 1)).slice(-2);
					var today = now.getFullYear()+"-"+(month)+"-"+(day);
					$('input[name="'+hffldname+'"]').val(today);
					$('input[name="'+hffldname+'"]').attr("min", today);
				}	
				else
					$('input[name="'+hffldname+'"]').val("-");
			}	
			
			$('select[name="'+hffldname+'"] option:eq(1)').attr('selected','selected');
			var fldnamecb = $(chkhfelem).find("input[type='checkbox']").attr('name');
			$('input[name="'+fldnamecb+'"]').prop( "checked", true );
		});
	}
	
	function conditionalHiddenFileFieldManage(reqattfilehfield) {
		reqattfilehfield.each(function(hfidx, hfelem) {
            if(!$(hfelem).val()) {
				$(hfelem).removeClass("wpcf7-validates-as-required");
            }
		});
	}
	
	function conditionalHiddenFieldrefresh(reqhidfield) {
		reqhidfield.each(function(chkhfidx, chkhfelem) {
            var hffldname = $(chkhfelem).attr('name');
			$('input[name="'+hffldname+'"]').val("");
			$('select[name="'+hffldname+'"] option:eq(1)').removeAttr('selected');
			$('textarea[name="'+hffldname+'"]').val("");
			var fldnamecb = $(chkhfelem).find("input[type='checkbox']").attr('name');
			$('input[name="'+fldnamecb+'"]').prop( "checked", false );
		});
	}
	
	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	
	function isPhone(phone) {
		var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
		if (filter.test(phone)) {
			var validphone = 1;
		}
		else{
			var validphone = 0;
		}
		return validphone;
	}
	
	function isUrlValid(url) {
		return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
	}
	
})(jQuery);

/*
|--------------------------------------------------------------------------
	End
|--------------------------------------------------------------------------
*/
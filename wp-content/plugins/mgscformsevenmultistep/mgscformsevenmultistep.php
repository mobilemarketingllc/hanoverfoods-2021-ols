<?php
/*
Plugin Name: Contact Form 7 Multi Step
Plugin URI: https://mgsdemo.mgscoder.com/wp-plugins/mgscf7-addon/contact-form-7-multi-step/
Description: Contact Form 7 Multi Step WordPress CF7 Add-ons. Split Long Form into Multi Step.
Version: 1.8.0
Author: MGScoder
Author URI: https://codecanyon.net/user/mgscoder?ref=mgscoder
Text Domain: mgscformsevenmultistep
Domain Path: /languages
*/
defined( 'ABSPATH' ) || exit;
define('MGS_CFORMSEVENMULTISTEP_VERSION', '1.8.0');
define('MGS_CFORMSEVENMULTISTEP_PLUGIN_DIR', str_replace('\\','/',dirname(__FILE__)));

if ( !class_exists( 'MgsCformSevenMultiStepInit' ) ) {

	class MgsCformSevenMultiStepInit {

		function __construct() {
			
			add_action( 'admin_notices', array( $this, 'mgscformsevenmultistep_admin_notices' ) );
			add_action( 'init', array( $this, 'mgscformsevenmultistep_init' ) );
			add_action( 'wpcf7_after_save', array( $this, 'mgscformsevenmultistep_save_settings' ) );
			add_action( 'wp_footer', array( $this, 'mgscformsevenmultistep_footer_fileenqueue' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'mgscformsevenmultistepadmin_style' ) );			
			add_action( 'wpcf7_editor_panels', array( $this, 'mgscformsevenmultistep_editor_panels' ) );
			add_action( 'wpcf7_add_meta_boxes', array( $this, 'mgscformsevenmultistep_add_settings' ) );
			add_action( 'wpcf7_init', array( $this, 'mgscformsevenmultistep_custom_shortcodes' ) );
			add_shortcode( 'wpcf7mgscfsmultistep-range-ux', array( $this, 'mgscformsevenmultistep_wpcf7_range_uxcode' ) );
			add_shortcode( 'wpcf7mgscfsmultistep-endux', array( $this, 'mgscformsevenmultistep_wpcf7_enduxcode' ) );
			add_shortcode( 'wpcf7mgscfsmultistep-confirmation', array( $this, 'mgscformsevenmultistep_wpcf7_confirmation' ) );
			
		}
			
		function mgscformsevenmultistep_admin_notices(){
			if ( !in_array( 'contact-form-7/wp-contact-form-7.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
				echo '<div class="error"><p>' . esc_html__('Contact Form 7 Multi Step  plugin requires active plugin', 'mgscformsevenmultistep') . ' <a href="//wordpress.org/plugins/contact-form-7/" target="_blank">' . esc_html__('Contact Form 7', 'mgscformsevenmultistep') . '</a></p></div>';
				
				$plugin = plugin_basename( __FILE__ );
				deactivate_plugins( $plugin );
			}
		}
		
		function mgscformsevenmultistep_init() {
			
			$domain = 'mgscformsevenmultistep';
			load_plugin_textdomain( $domain, FALSE, basename( dirname( __FILE__ ) ) . '/languages' );
			
		}
		
		function mgscformsevenmultistep_footer_fileenqueue() {
			
			$mgscformsevenmultistep_ver = '1.8.0';
			
			wp_enqueue_style('fontawesome', plugin_dir_url( __FILE__ ) . 'css/fontawesome/all.min.css');
			wp_enqueue_style('mgscformsevenmultistep', plugin_dir_url( __FILE__ ) . 'css/mgscformsevenmultistep.css');			
			wp_enqueue_script('mgscformsevenmultistepjs', plugin_dir_url( __FILE__ ) . 'js/mgscformsevenmultistep.js', array('jquery'));
						
			// Localize the script with new data
			$mgscfsmultistep_translate_array = array(
				'field_required' => __( 'The field is required.', 'mgscformsevenmultistep' ),
				'email_invalid' => __( 'The e-mail address entered is invalid. ', 'mgscformsevenmultistep' ),
				'tel_invalid' => __( 'The telephone number is invalid. ', 'mgscformsevenmultistep' ),
				'url_invalid' => __( 'The URL is invalid. ', 'mgscformsevenmultistep' ),
				'fields_error' => __( 'One or more fields have an error. Please check and try again.!!!', 'mgscformsevenmultistep' )
			);
			wp_localize_script( 'mgscformsevenmultistepjs', 'mgscfsmultistep_translate_string', $mgscfsmultistep_translate_array );
			
		}		
		
		function mgscformsevenmultistepadmin_style($hook) {
			
			$mgscformsevenmultistep_ver = '1.8.0';
			if ( $hook != 'toplevel_page_wpcf7' ) {
				return;
			}
			
			wp_enqueue_style('mgscformsevenmultistepadmin', plugin_dir_url( __FILE__ ) . 'css/mgscformsevenmultistepadmin.css');
			wp_enqueue_script('mgscformsevenmultistepadminjs', plugin_dir_url( __FILE__ ) . 'js/mgscformsevenmultistepadmin.js', array('jquery'), '', true);
			
		}		
		
		function mgscformsevenmultistep_save_settings( $args ) {
			
			update_option( 'wpcf7_mgscfsmultistepenable_'. $args->id(), $_POST['wpcf7mgscfsmultistepenable'] );
			
		}
		
		function mgscformsevenmultistep_editor_panels ( $panels ) {
			
			$panels['mgscformsevenmultistep-panel'] = array(
				'title' => esc_html__( 'Multi Step', 'mgscformsevenmultistep' ),
				'callback' => array( $this, 'mgscformsevenmultistep_render_metabox') );		
			return $panels;
		}
		
		function mgscformsevenmultistep_add_settings() {
			add_meta_box( 'wpcf7pma_form_options', esc_html__('Multi Step', 'mgscformsevenmultistep'), array( $this, 'mgscformsevenmultistep_render_metabox' ), null, 'mail' );
		}
		
		function mgscformsevenmultistep_render_metabox( $args ) {
			$wpcf7mgscfsmultistepenable = get_option( 'wpcf7_mgscfsmultistepenable_' . $args->id() );			
			require_once(MGS_CFORMSEVENMULTISTEP_PLUGIN_DIR . '/inc/mgscformsevenmultistep-options.php');
		}
		
		function mgscformsevenmultistep_custom_shortcodes(){
			add_filter( 'wpcf7_form_elements', 'do_shortcode' );
		}
		
		function mgscformsevenmultistep_wpcf7_range_uxcode( $atts ) {
			add_filter( 'wpcf7_form_elements', 'do_shortcode' );
			$mgswpcf7multisteprangeux = '';
			$mgswpcf7multistepcstep = $atts["cstep"];
			$mgswpcf7multisteptotalstep = $atts["totalsteps"];
			$mgswpcf7multisteptheme = $atts["mgswpcfstheme"];			
			if($mgswpcf7multisteptheme) {
				$mgswpcf7multisteptheme = ' mgswpcf7multisteptheme'. $mgswpcf7multisteptheme;
			}
			else {
				$mgswpcf7multisteptheme = ' mgswpcf7multisteptheme1';
			}
			
			$mgswpcf7multistepallstepstitle = explode("|", $atts["stepstitle"]);
			
			$mgswpcf7multistepareyoureadytext = ($atts["areyoureadytext"]) ? $atts["areyoureadytext"] : esc_html__("Are You ready!", "mgscformsevenmultistep");
			$mgswpcf7multistepnextbtntext = ($atts["nextbtntext"]) ? $atts["nextbtntext"] : esc_html__("Next", "mgscformsevenmultistep");
			$mgswpcf7multistepbackbtntext = ($atts["backbtntext"]) ? $atts["backbtntext"] : esc_html__("Back", "mgscformsevenmultistep");
			$mgswpcf7multistepnearlytheretext = ($atts["nearlytheretext"]) ? $atts["nearlytheretext"] : esc_html__("You Are Nearly There", "mgscformsevenmultistep");
						
			$mgswpcf7multistepbackstep = $atts["cstep"] - 2;
			$mgswpcf7multistepnextstep = $atts["cstep"];
			
			$mgswpcf7msconfprvstep = $mgswpcf7multisteptotalstep;
			if($mgswpcf7multistepcstep == $mgswpcf7msconfprvstep || $mgswpcf7multisteptotalstep == 2){
				$mgswpcf7msconfprvstepbtnclass = ' mgswpcf7msgoconfirmstep';
			}	
			else{
				$mgswpcf7msconfprvstepbtnclass = '';
			}
			
			if($mgswpcf7multistepcstep != 1){
				$mgswpcf7multistepslideright = ' slide-right';
				$mgswpcf7multisteprangeux .= '<div class="help-block with-errors mandatory-error displerrorbot"></div></div>';
			}
			$mgswpcf7multistepdone = round( ((100/$mgswpcf7multisteptotalstep)*$mgswpcf7multistepcstep), 2 ) .'%';
			
			if($mgswpcf7multistepcstep == 1){
				$formlayerprogresslinep = round( (((100/$mgswpcf7multisteptotalstep)/2)), 2 );
			}
			else{
				$formlayerprogresslinep = round( (((100/$mgswpcf7multisteptotalstep)*$mgswpcf7multistepcstep) - ((100/$mgswpcf7multisteptotalstep)/2)), 2 );
			}
			$formlayerprogresslinedone = 'width: '.$formlayerprogresslinep.'%;';
						
			if($mgswpcf7multistepcstep == 1){
				$mgswpcf7multisteprangeux .= '';
			}
			elseif($mgswpcf7multistepcstep == 2){
				$mgswpcf7multisteprangeux .= '<div class="mgswpcf7multistep-btn-group mgswpcf7multistep-btn-box-1">
											<button class="btn mgswpcf7btn-default disable" type="button">'. esc_html__("$mgswpcf7multistepareyoureadytext", "mgscformsevenmultistep") .'</button>
											<button class="btn mgswpcf7btn-custom pull-right mgscfsMultiStepNextStep2'. $mgswpcf7msconfprvstepbtnclass .'" type="button">'. esc_html__("$mgswpcf7multistepnextbtntext", "mgscformsevenmultistep") .' <i class="fas fa-arrow-right"></i></button> 
										</div>';
			}
			else{
				$mgswpcf7multisteprangeux .= '<div class="mgswpcf7multistep-btn-group mgswpcf7multistep-btn-box-'. $mgswpcf7multistepcstep . $mgswpcf7multistepslideright .'">
											<button class="btn mgswpcf7btn-default mgscfsMultiStepPreviousStep'. $mgswpcf7multistepbackstep .'" type="button"><i class="fas fa-arrow-left"></i> '. esc_html__("$mgswpcf7multistepbackbtntext", "mgscformsevenmultistep") .'</button>
											<button class="btn mgswpcf7btn-custom pull-right mgscfsMultiStepNextStep'. $mgswpcf7multistepnextstep . $mgswpcf7msconfprvstepbtnclass .'" type="button">'. esc_html__("$mgswpcf7multistepnextbtntext", "mgscformsevenmultistep") .' <i class="fas fa-arrow-right"></i></button> 
										</div>';
			}
			
			$mgswpcf7multisteprangeux .= '<div id="mgswpcf7multistepsection-'. $mgswpcf7multistepcstep .'" class="mgswpcf7multistepsection'. $mgswpcf7multistepslideright .'">';
			$mgswpcf7multisteprangeux .= '<h5 class="mgswpcf7multistepsection-tagline">'. esc_html__("$mgswpcf7multistepnearlytheretext", "mgscformsevenmultistep") .'</h5>
			<h3 class="mgswpcf7multistepsection-title">'. esc_html__("Step ", "mgscformsevenmultistep") . esc_html__("$mgswpcf7multistepcstep", "mgscformsevenmultistep") . esc_html__(" of ", "mgscformsevenmultistep") . esc_html__("$mgswpcf7multisteptotalstep", "mgscformsevenmultistep") .' <em>( '. esc_html__("$mgswpcf7multistepdone", "mgscformsevenmultistep") .' )</em></h3>';
			$mgswpcf7multisteprangeux .= '<div class="mgswpcf7multistepform-layer-steps form-layer-tolal-steps-'. $mgswpcf7multisteptotalstep . $mgswpcf7multisteptheme .'">
				<div class="form-layer-progress">
					<div class="form-layer-progress-line" style="'.$formlayerprogresslinedone.'"></div>
				</div>';
				
				for ($ms = 1; $ms <= $mgswpcf7multisteptotalstep; $ms++) {
					if( ($ms == 1 && $mgswpcf7multistepcstep == 1) || $ms == $mgswpcf7multistepcstep){
						$mgswpcf7multistepactivestep = ' activestep';
					}
					elseif($ms < $mgswpcf7multistepcstep){
						$mgswpcf7multistepactivestep = ' active';
					}	
					else{
						$mgswpcf7multistepactivestep = '';
					}	
					
					$stepiconornum = esc_html__("$ms", "mgscformsevenmultistep");
					
					$mgswpcf7mstepcsteptarr = $ms-1;
					if($mgswpcf7multistepallstepstitle[$mgswpcf7mstepcsteptarr])
						$mgswpcf7multistepcsteptitle = '<p class="steptitle">'. esc_html__("$mgswpcf7multistepallstepstitle[$mgswpcf7mstepcsteptarr]", "mgscformsevenmultistep") .'</p>';
										
					$mgswpcf7multisteprangeux .= '<div class="form-layer-step'. $mgswpcf7multistepactivestep .'">
						<div class="form-layer-step-icon mgscfsstep"><span>'. $stepiconornum .'</span></div>'. $mgswpcf7multistepcsteptitle .'
					</div>';
				}
			$mgswpcf7multisteprangeux .= '</div>					
			<div class="help-block with-errors mandatory-error displerrortop"></div>';
			
			return $mgswpcf7multisteprangeux;			
		}		
		
		function mgscformsevenmultistep_wpcf7_enduxcode( $atts ) {
			$mgswpcf7multisteptotalstep = $atts["totalsteps"];		
			$mgswpcf7multistepbackstep = $atts["totalsteps"]-1;
			$mgswpcf7multistepbackbtntext = ($atts["backbtntext"]) ? $atts["backbtntext"] : esc_html__("Back", "mgscformsevenmultistep");
			
			$mgswpcf7multistependuxc = '';
			if($mgswpcf7multisteptotalstep > 1){
				$mgswpcf7multistependuxc .= '<div class="mgswpcf7multistep-btn-group mgswpcf7msfinalstepbtn">
												<button class="btn mgswpcf7btn-default mgscfsMultiStepPreviousStep'. $mgswpcf7multistepbackstep .'" type="button"><i class="fas fa-arrow-left"></i> '. esc_html__("$mgswpcf7multistepbackbtntext", "mgscformsevenmultistep") .'</button>
											</div>';
			}
			$mgswpcf7multistependuxc .= '</div>					
				<div class="help-block with-errors mandatory-error"></div>';
			
			return $mgswpcf7multistependuxc;			
		}
		
		function mgscformsevenmultistep_wpcf7_confirmation( $atts ) {
			$mgswpcf7multisteplabelnfname = $atts["labelnfname"];
			$mgswpcf7multistepconfbox = '<div class="mgswpcf7multistep-toggle-icon">
							<a class="toggle-get-mgswpcf7multistep-btn" href="javaScript:Void(0);">
								'. esc_html__("Your Input", "mgscformsevenmultistep") .' <i class="far fa-arrow-alt-circle-down"></i>
							</a>
						</div>';
			$mgswpcf7multistepconfbox .= '<div class="mgswpcf7multistep-confirmation-box">';
			$mgswpcf7msteplnfnvaluestr = "";
			
			$mgswpcf7multisteplabelnfnamearr = explode("|", $mgswpcf7multisteplabelnfname);
			if($mgswpcf7multisteplabelnfnamearr){
				foreach ($mgswpcf7multisteplabelnfnamearr as $mgswpcf7msteplnfn) {
					$mgswpcf7msteplnfnarr = explode("::", $mgswpcf7msteplnfn);
					$mgswpcf7multistepconfbox .= '<div class="mgswpcf7multistep-confirmation-field">';
					
					foreach ($mgswpcf7msteplnfnarr as $mgswpcf7msteplnfnkey => $mgswpcf7msteplnfnvalue) {
						
						if($mgswpcf7msteplnfnkey == 0)
							$mgswpcf7multistepconfbox .= $mgswpcf7msteplnfnvalue .': ';
						else{
							$mgswpcf7multistepconfbox .= '<span id="' .$mgswpcf7msteplnfnvalue. '"></span>';
							
							$mgswpcf7msteplnfnvaluestr .= $mgswpcf7msteplnfnvalue .'|';
						}
						
					}
					$mgswpcf7multistepconfbox .= '</div>';
				}
				$mgswpcf7multistepconfbox .= '<input type="hidden" id="mgswpcf7msteplnfnvaluestr" nme="mgswpcf7msteplnfnvaluestr" value="' .$mgswpcf7msteplnfnvaluestr. '">';
				$mgswpcf7multistepconfbox .= '</div>';
			}
			
			return $mgswpcf7multistepconfbox;			
		}
				
	}
	$mgscformsevenmultistep = new MgsCformSevenMultiStepInit();
}
		
?>

<?php
/*
Plugin Name: Contact Form 7 Multi Step
Plugin URI: https://mgsdemo.mgscoder.com/wp-plugins/mgscf7-addon/contact-form-7-multi-step/
Description: Contact Form 7 Multi Step WordPress CF7 Add-ons. Split Long Form into Multi Step.
Author: MGScoder
Author URI: https://codecanyon.net/user/mgscoder?ref=mgscoder
Text Domain: mgscformsevenmultistep
Domain Path: /languages
*/
defined( 'ABSPATH' ) || exit;
?>

<div class="postbox">
	<h3 class="hndle"><?php esc_html_e( 'Need Support?', 'mgscformsevenmultistep'); ?></h3>
	<div class="inside">
		<p><?php esc_html_e( 'If Need any assist then plaese Contact through Codecanyon Item page Support tab. Support Team will be very happy to assist you.', 'mgscformsevenmultistep'); ?></p>
		<p>
			<a href="<?php echo esc_url( "//codecanyon.net/item/contact-form-7-multi-step-split-long-form-into-multi-step/24652301/support" ); ?>" class="button" target="_blank"><?php esc_html_e('Support', 'mgscformsevenmultistep'); ?></a>
			<a href="<?php echo esc_url( "//docs.mgscoder.com/mgscformsevenmultistep-documentation/" ); ?>" class="button" target="_blank"><?php esc_html_e('Documentation', 'mgscformsevenmultistep'); ?></a>
		</p>
	</div>
</div>

<div class="postbox">
	<h3 class="hndle"><?php esc_html_e( 'Rate 5 Stars', 'mgscformsevenmultistep'); ?></h3>
	<div class="inside">
		<p><?php esc_html_e( 'Like this Plugin Rate it 5 stars with Nice Comments. Your Great Rating will Appreciate to create More Useful Plugins.', 'mgscformsevenmultistep'); ?></p>
		<p><a href="<?php echo esc_url( "//codecanyon.net/downloads" ); ?>" class="button" target="_blank"><?php esc_html_e( 'Rate 5 Stars', 'mgscformsevenmultistep'); ?></a></p>
	</div>
</div>

<div class="postbox">
	<h3 class="hndle"><?php esc_html_e( 'Follow Us', 'mgscformsevenmultistep'); ?></h3>
	<div class="inside">
		<p><?php esc_html_e( 'Like to get our Latest Product Notification Follow Us.', 'mgscformsevenmultistep'); ?></p>
		<p>
			<a href="<?php echo esc_url( "//codecanyon.net/user/mgscoder/follow" ); ?>" class="button" target="_blank"><?php esc_html_e( 'Follow Us', 'mgscformsevenmultistep'); ?></a>
			<a href="<?php echo esc_url( "//codecanyon.net/item/contact-form-7-multi-step-split-long-form-into-multi-step/24652301?ref=mgscoder" ); ?>" class="button" target="_blank"><?php esc_html_e( 'Buy Now', 'mgscformsevenmultistep'); ?></a>
		</p>
	</div>
</div>

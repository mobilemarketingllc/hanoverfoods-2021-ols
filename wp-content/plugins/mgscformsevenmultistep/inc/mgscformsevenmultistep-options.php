<?php
/*
Plugin Name: Contact Form 7 Multi Step
Plugin URI: https://mgsdemo.mgscoder.com/wp-plugins/mgscf7-addon/contact-form-7-multi-step/
Description: Contact Form 7 Multi Step WordPress CF7 Add-ons. Split Long Form into Multi Step.
Author: MGScoder
Author URI: https://codecanyon.net/user/mgscoder?ref=mgscoder
Text Domain: mgscformsevenmultistep
Domain Path: /languages
*/
defined( 'ABSPATH' ) || exit;
?>
<div class="wrap">
	<h3><?php esc_html_e( 'Contact Form 7 Multi Step Settings', 'mgscformsevenmultistep'); ?></h3>
	
	<div id="mgscformsevenmultistep-settings-panel">
		<div class="inside">
				
			<div id="mgscformsevenmultistepformc" class="form-section">
				<h5><?php esc_html_e( 'Contact Form 7 Multi Step Enable:', 'mgscformsevenmultistep'); ?></h5>
				<p><input name="wpcf7mgscfsmultistepenable" id="wpcf7mgscfsmultistepenable" class="form-control" type="checkbox" value="1" checked disabled> <?php esc_html_e( 'Enable', 'mgscformsevenmultistep'); ?></p>
				
				<div id="mgscfssmultistepbasiccs" class="form-section">
					<label for="wpcf7-shortcode"><?php esc_html_e( 'At the begining of each step You have to add shortcode as like the Following [at the very begining of each step]', 'mgscformsevenmultistep'); ?></label>
					<input name="wpcf7mgscfsmultisteprangeshortcode" id="wpcf7mgscfsmultisteprangeshortcode" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='1' totalsteps='4']" readonly>
					<p>
						<em class="note"><?php esc_html_e( 'Here', 'mgscformsevenmultistep'); ?></em>
						<em class="note"><code><?php esc_html_e( 'cstep', 'mgscformsevenmultistep'); ?></code></em>
						<em class="note"><?php esc_html_e( 'is the current step and', 'mgscformsevenmultistep'); ?></em>
						<em class="note"><code><?php esc_html_e( 'totalsteps', 'mgscformsevenmultistep'); ?></code></em>
						<em class="note"><?php esc_html_e( 'is the Total Steps', 'mgscformsevenmultistep'); ?></em>
						<em class="note"><code><?php esc_html_e( '[Do not forget to change totalsteps value]:', 'mgscformsevenmultistep'); ?></code></em>
					</p>
					<label for="wpcf7-shortcode"><?php esc_html_e( 'At the end of the form mean after Send/Submit button You have to add the following Shortcode', 'mgscformsevenmultistep'); ?></label>
					<input name="wpcf7mgscfsmultistependuxshortcode" id="wpcf7mgscfsmultistependuxshortcode" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-endux totalsteps='4']" readonly>
					<p><em class="note"><?php esc_html_e( 'Add the above shortcode at the end of the form. Do not forget to put the exact Total Steps Number', 'mgscformsevenmultistep'); ?></em></p>
					
				</div>
			</div>
			<hr />
			<hr />
			
			<h3><?php esc_html_e( 'Contact Form 7 Multi Step Settings Management', 'mgscformsevenmultistep'); ?></h3>
			<ul class="radio-buttons radio-buttons-group-column  list-unlist">
				<li>
					<input name="mgscf7multistepsetting" id="mgscf7multistepsetting1" class="form-control" type="radio" value="defaultSettings" checked> <?php esc_html_e( 'Default (Start here)', 'mgscformsevenmultistep'); ?> &nbsp;
					<input name="mgscf7multistepsetting" id="mgscf7multistepsetting2" class="form-control" type="radio" value="displayStepTitle"> <?php esc_html_e( 'Display Step Title', 'mgscformsevenmultistep'); ?> &nbsp;
					<input name="mgscf7multistepsetting" id="mgscf7multistepsetting3" class="form-control" type="radio" value="changeButtonText"> <?php esc_html_e( 'Change Button Text', 'mgscformsevenmultistep'); ?> &nbsp;
					<input name="mgscf7multistepsetting" id="mgscf7multistepsetting4" class="form-control" type="radio" value="dsiplayFormInput"> <?php esc_html_e( 'Dsiplay Form Input (Confirmation)', 'mgscformsevenmultistep'); ?> &nbsp;
					<input name="mgscf7multistepsetting" id="mgscf7multistepsetting5" class="form-control" type="radio" value="mgscf7themeSettings"> <?php esc_html_e( 'Theme Settings', 'mgscformsevenmultistep'); ?>
					<input name="mgscf7multistepsetting" id="mgscf7multistepsetting6" class="form-control" type="radio" value="cssSettings"> <?php esc_html_e( 'CSS Settings (Optional)', 'mgscformsevenmultistep'); ?>
				</li>
			</ul><!-- .radio-buttons -->
			<hr />
			
			<div id="defaultSettings">
				<div class="description">
					<h5>
						<?php esc_html_e( 'You can use the following shortcode for required step ', 'mgscformsevenmultistep'); ?>
						<em class="note"><code><?php esc_html_e( '[Do not forget to change totalsteps value]:', 'mgscformsevenmultistep'); ?></code></em>
					</h5>
					<p><em class="note"><?php esc_html_e( 'Step 1 [Paste at the very begining of step 1]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode1" id="wpcf7mgscfsmultisteprangeshortcode1" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='1' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 2 [Paste at the very begining of step 2]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode2" id="wpcf7mgscfsmultisteprangeshortcode2" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='2' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 3 [Paste at the very begining of step 3]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode3" id="wpcf7mgscfsmultisteprangeshortcode3" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='3' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 4 [Paste at the very begining of step 4]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode4" id="wpcf7mgscfsmultisteprangeshortcode4" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='4' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 5 [Paste at the very begining of step 5]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode5" id="wpcf7mgscfsmultisteprangeshortcode5" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='5' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 6 [Paste at the very begining of step 6]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode6" id="wpcf7mgscfsmultisteprangeshortcode6" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='6' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 7 [Paste at the very begining of step 7]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode7" id="wpcf7mgscfsmultisteprangeshortcode7" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='7' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 8 [Paste at the very begining of step 8]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode8" id="wpcf7mgscfsmultisteprangeshortcode8" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='8' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 9 [Paste at the very begining of step 9]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode9" id="wpcf7mgscfsmultisteprangeshortcode9" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='9' totalsteps='10']" readonly>
					<p><em class="note"><?php esc_html_e( 'Step 10 [Paste at the very begining of step 10]', 'mgscformsevenmultistep'); ?></em></p>
					<input name="wpcf7mgscfsmultisteprangeshortcode10" id="wpcf7mgscfsmultisteprangeshortcode10" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='10' totalsteps='10']" readonly>
				</div>
				<hr />
				<h5><?php esc_html_e( 'Check the following image as an example how to use the step Shortcode in your Form:', 'mgscformsevenmultistep'); ?></h5>
				<img src="<?php echo esc_url( plugins_url( 'images/mgscformsevenmultistep-settings.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'shortcode structure', 'mgscformsevenmultistep'); ?>" width="97%" />
				<hr />
			</div>
			
			<div id="displayStepTitle">
				<h3 class="yellowbg"><?php esc_html_e( 'How to Display Step Title', 'mgscformsevenmultistep'); ?></h3>
				<p class="note"><?php esc_html_e( 'You have to Add the Steps Title with the following code in each step Shortcode', 'mgscformsevenmultistep'); ?></p>
				<p class="note"><?php esc_html_e( 'Change Steps Title (Step1|Step2|Step3......) according to your choice', 'mgscformsevenmultistep'); ?></p>
<pre>
stepstitle='Step1|Step2|Step3|Step4|Step5|Step6|Step7|Step8|Step9|Step10'
</pre>
				<h5><?php esc_html_e( 'Example:', 'mgscformsevenmultistep'); ?></h5>
				<input name="wpcf7mgscfsmultisteprangeshortcodeexamp" id="wpcf7mgscfsmultisteprangeshortcodeexamp" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='1' stepstitle='Step1|Step2|Step3|Step4|Step5|Step6|Step7|Step8|Step9|Step10' totalsteps='10']" readonly>
				<h5><?php esc_html_e( 'Check the following image as an example how to use the step Shortcode in your Form:', 'mgscformsevenmultistep'); ?></h5>
				<img src="<?php echo esc_url( plugins_url( 'images/mgscformsevenmultistep-title.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'shortcode structure', 'mgscformsevenmultistep'); ?>" width="97%" />
				<hr />
			</div>
			
			<div id="changeButtonText">
				<h3 class="yellowbg"><?php esc_html_e( 'How to Change Button Text', 'mgscformsevenmultistep'); ?></h3>
				<p class="note"><?php esc_html_e( 'You have to Add the Button Text with the following code in each step Shortcode', 'mgscformsevenmultistep'); ?></p>
<pre>
nearlytheretext='You Are Nearly There'
</pre>
<pre>
areyoureadytext='Are You ready!'
</pre>
<pre>
nextbtntext='Next'
</pre>
<pre>
backbtntext='Back'
</pre>
				<h5><?php esc_html_e( 'Example:', 'mgscformsevenmultistep'); ?></h5>
				<input name="wpcf7mgscfsmultisteprangeshortcodebtntext" id="wpcf7mgscfsmultisteprangeshortcodebtntext" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='1' nearlytheretext='You Are Nearly There!' totalsteps='10']" readonly>
				<input name="wpcf7mgscfsmultisteprangeshortcodebtntext2" id="wpcf7mgscfsmultisteprangeshortcodebtntext2" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='2' nearlytheretext='You Are Step 2 Now!' areyoureadytext='Are You Ready!' nextbtntext='Next' totalsteps='10']" readonly>
				<input name="wpcf7mgscfsmultisteprangeshortcodebtntext3" id="wpcf7mgscfsmultisteprangeshortcodebtntext3" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='3' nearlytheretext='You Are Step 3 Now!' nextbtntext='Next' backbtntext='Back' totalsteps='10']" readonly>
				<h5 class="yellowbg"><?php esc_html_e( 'After the Form Send/Submit button:', 'mgscformsevenmultistep'); ?></h5>
				<input name="wpcf7mgscfsmultisteprangeshortcodebtntextend" id="wpcf7mgscfsmultisteprangeshortcodebtntextend" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-endux backbtntext='Back' totalsteps='10']" readonly>
				<ol class="yellowbg">
					<li><?php esc_html_e( "cstep='1' there is no nextbtntext and backbtntext only nearlytheretext", 'mgscformsevenmultistep'); ?></li>
					<li><?php esc_html_e( "cstep='2' there is no backbtntext instead areyoureadytext", 'mgscformsevenmultistep'); ?></li>
					<li><?php esc_html_e( "cstep='3' to last cstep='..' can be both nextbtntext &amp; backbtntext (if you need to change)", 'mgscformsevenmultistep'); ?></li>
					<li><?php esc_html_e( "after the Send/Submit button in the last shortcode there is no nextbtntext", 'mgscformsevenmultistep'); ?></li>
				</ol>
				<h5><?php esc_html_e( 'Check the following image as an example how to use the step Shortcode in your Form:', 'mgscformsevenmultistep'); ?></h5>
				<img src="<?php echo esc_url( plugins_url( 'images/mgscformsevenmultistep-btn-settings.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'shortcode structure', 'mgscformsevenmultistep'); ?>" width="97%" />
				<hr />
			</div>
			
			<div id="dsiplayFormInput">
				<h3 class="yellowbg"><?php esc_html_e( 'How to Dsiplay Form Input (Confirmation and Submit)', 'mgscformsevenmultistep'); ?></h3>
				<p class="note"><?php esc_html_e( 'You have to Add the Confirmation Shortcode just above the Send/Submit button in the last step.', 'mgscformsevenmultistep'); ?></p>
				<p><?php esc_html_e( 'The following Confirmation Shortcode is an example for you.', 'mgscformsevenmultistep'); ?></p>
				<p class="note"><?php esc_html_e( 'in Confirmation Shortcode labelnfname property you have to provide all of your form Field Label and Field name.', 'mgscformsevenmultistep'); ?></p>
				<p><?php esc_html_e( 'The following screenshot code is an example for you. Please check how to manage the Confirmation shortcode.', 'mgscformsevenmultistep'); ?></p>
				<img src="<?php echo esc_url( plugins_url( 'images/mgscformsevenmultistep-confirmation.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'shortcode structure', 'mgscformsevenmultistep'); ?>" width="97%" />
				<p class="note"><?php esc_html_e( 'Separator (::) between Field Label and Field Name.', 'mgscformsevenmultistep'); ?></p>
				<p class="note"><?php esc_html_e( 'Separator (|) between each Field Property (Field Label and Field Name).', 'mgscformsevenmultistep'); ?></p>
				<hr />
				<h5><?php esc_html_e( 'Example Confirmation Shortcode:', 'mgscformsevenmultistep'); ?></h5>
				<textarea name="dsiplayforminputshortcode" id="dsiplayforminputshortcode" rows="3" class="form-control large-text code" onFocus="this.select();">[wpcf7mgscfsmultistep-confirmation labelnfname='Select OS::opsys|Select Features::features|Project Budget::probudget|Your Name::your-name|Your Email::your-email|']</textarea>
				<hr />
			</div>
			
			<div id="mgscf7themeSettings">
				<h3 class="yellowbg"><?php esc_html_e( 'How to Setup Theme', 'mgscformsevenmultistep'); ?></h3>
				<p class="note"><?php esc_html_e( 'We have currently 6 themes', 'mgscformsevenmultistep'); ?></p>
				<p class="note"><?php esc_html_e( 'Theme 1 default theme and you do not need to do anything for this theme.', 'mgscformsevenmultistep'); ?></p>
				<p><img src="<?php echo esc_url( plugins_url( 'images/mgswpcf7multisteptheme1.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Theme 1', 'mgscformsevenmultistep'); ?>" width="97%" /></p>
				<p class="note"><?php esc_html_e( 'Theme 2 - you have to set: mgswpcfstheme and value 2', 'mgscformsevenmultistep'); ?></p>
				<p><img src="<?php echo esc_url( plugins_url( 'images/mgswpcf7multisteptheme2.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Theme 2', 'mgscformsevenmultistep'); ?>" width="97%" /></p>
				<p class="note"><?php esc_html_e( 'Theme 3 - you have to set: mgswpcfstheme and value 3', 'mgscformsevenmultistep'); ?></p>
				<p class="note"><?php esc_html_e( 'You can choose this theme 3 if the steps are many more', 'mgscformsevenmultistep'); ?></p>
				<p><img src="<?php echo esc_url( plugins_url( 'images/mgswpcf7multisteptheme3.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Theme 3', 'mgscformsevenmultistep'); ?>" width="97%" /></p>
				<p class="note"><?php esc_html_e( 'Theme 4 - you have to set: mgswpcfstheme and value 4', 'mgscformsevenmultistep'); ?></p>
				<p><img src="<?php echo esc_url( plugins_url( 'images/mgswpcf7multisteptheme4.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Theme 4', 'mgscformsevenmultistep'); ?>" width="97%" /></p>
				<p class="note"><?php esc_html_e( 'Theme 5 - you have to set: mgswpcfstheme and value 5', 'mgscformsevenmultistep'); ?></p>
				<p><img src="<?php echo esc_url( plugins_url( 'images/mgswpcf7multisteptheme5.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Theme 5', 'mgscformsevenmultistep'); ?>" width="97%" /></p>
				<p class="note"><?php esc_html_e( 'Theme 6 - you have to set: mgswpcfstheme and value 6', 'mgscformsevenmultistep'); ?></p>
				<p><img src="<?php echo esc_url( plugins_url( 'images/mgswpcf7multisteptheme6.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Theme 6', 'mgscformsevenmultistep'); ?>" width="97%" /></p>
				<p class="note"><?php esc_html_e( 'Theme 7 - you have to set: mgswpcfstheme and value 7', 'mgscformsevenmultistep'); ?></p>
				<p><img src="<?php echo esc_url( plugins_url( 'images/mgswpcf7multisteptheme7.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Theme 7', 'mgscformsevenmultistep'); ?>" width="97%" /></p>
				
				<p class="note"><?php esc_html_e( 'You have to Setup Theme by adding the following code in each step Shortcode (if you like to change theme)', 'mgscformsevenmultistep'); ?></p>
<pre>
mgswpcfstheme='2'
</pre>
<pre>
mgswpcfstheme='3'
</pre>
<pre>
mgswpcfstheme='4'
</pre>
<pre>
mgswpcfstheme='5'
</pre>
<pre>
mgswpcfstheme='6'
</pre>
<pre>
mgswpcfstheme='7'
</pre>
				<h5><?php esc_html_e( 'Example:', 'mgscformsevenmultistep'); ?></h5>
				<input name="wpcf7mgscfsmultisteptheme" id="wpcf7mgscfsmultisteptheme" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='1' mgswpcfstheme='6' stepstitle='Service|Budget|Info|Requirement|Confirmation' totalsteps='5']" readonly>
				<input name="wpcf7mgscfsmultisteptheme2" id="wpcf7mgscfsmultisteptheme2" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='2' mgswpcfstheme='6' stepstitle='Service|Budget|Info|Requirement|Confirmation' totalsteps='5']" readonly>
				<input name="wpcf7mgscfsmultisteptheme3" id="wpcf7mgscfsmultisteptheme3" class="form-control large-text code" onFocus="this.select();" type="text" value="[wpcf7mgscfsmultistep-range-ux cstep='3' mgswpcfstheme='6' stepstitle='Service|Budget|Info|Requirement|Confirmation' totalsteps='5']" readonly>
				<hr />
			</div>
			
			<div id="cssSettings">
				<h3 class="yellowbg"><?php esc_html_e( 'CSS Settings (Optional)', 'mgscformsevenmultistep'); ?></h3>
				<p><span class="yellowbg"><?php esc_html_e( '[You can add in your theme style css]', 'mgscformsevenmultistep'); ?></span></p>
				<p class="note"><?php esc_html_e( 'Add the following css in your theme style css to hide Form Top error', 'mgscformsevenmultistep'); ?></p>
<pre>
.help-block.with-errors.mandatory-error.displerrortop {
   display: none;
}
</pre>
				<p class="note"><?php esc_html_e( 'Add the following css in your theme style css to hide Form Bottom error', 'mgscformsevenmultistep'); ?></p>
<pre>
.help-block.with-errors.mandatory-error.displerrorbot {
   display: none;
}
</pre>
				<img src="<?php echo esc_url( plugins_url( 'images/form-error-displ.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'form error', 'mgscformsevenmultistep'); ?>" width="97%" />
				<hr />
				
				<p><span class="yellowbg"><?php esc_html_e( '[You can add in your theme style css]', 'mgscformsevenmultistep'); ?></span></p>
				<p class="note"><?php esc_html_e( 'Customize the following css to change the Steps Number (1,2,3,...) Style and add in your theme style css to make effect', 'mgscformsevenmultistep'); ?></p>
<pre>
.wpcf7-form .mgswpcf7multistepsection .form-layer-step.active .form-layer-step-icon {
	background-color: #1c20e2;
}
</pre>

				<p class="note"><?php esc_html_e( 'Customize the following css to change the Form Button (Next/Back) Style and add in your theme style css to make effect', 'mgscformsevenmultistep'); ?></p>
<pre>
.wpcf7-form .mgswpcf7multistep-btn-group .btn {   
	font-size:18px;
	padding: 6px 25px;
	border-radius: 3px;
}
.wpcf7-form .mgswpcf7multistep-btn-group .mgswpcf7btn-custom {
	background-color: rgba(3, 8, 223, 0.9);
	border: 1px solid rgba(3, 8, 223, 0.9);
	color: #fff;
}
.wpcf7-form .mgswpcf7multistep-btn-group .mgswpcf7btn-custom::before {
	background-color: #fff;
}
.wpcf7-form .mgswpcf7multistep-btn-group .mgswpcf7btn-custom:hover, 
.wpcf7-form .mgswpcf7multistep-btn-group .mgswpcf7btn-custom:focus, 
.wpcf7-form .mgswpcf7multistep-btn-group .mgswpcf7btn-custom:active {
	border-color: #0505f5;
	color: #000;
}
</pre>
				<hr />
				<img src="<?php echo esc_url( plugins_url( 'images/steps-buttons.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'Steps Buttons', 'mgscformsevenmultistep'); ?>" width="97%" />
				<p class="note"><strong><?php esc_html_e( 'You can hide the above Steps Buttons by choosing theme (Theme Settings: Theme 3)', 'mgscformsevenmultistep'); ?></strong></p>
				<p><span class="yellowbg"><?php esc_html_e( '[You can add in your theme style css]', 'mgscformsevenmultistep'); ?></span></p>
				<p class="note"><?php esc_html_e( 'Also you can add the following css in your theme style css to hide the above Steps Buttons', 'mgscformsevenmultistep'); ?></p>
<pre>
.wpcf7-form .mgswpcf7multistepform-layer-steps {   
	display: none;
}
</pre>
				<hr />
				<img src="<?php echo esc_url( plugins_url( 'images/percentage-displ.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'percentage display', 'mgscformsevenmultistep'); ?>" width="97%" />
				<p><span class="yellowbg"><?php esc_html_e( '[You can add in your theme style css]', 'mgscformsevenmultistep'); ?></span></p>
				<p class="note"><?php esc_html_e( 'Add the following css in your theme style css to hide the above Percentage', 'mgscformsevenmultistep'); ?></p>
<pre>
.wpcf7-form .mgswpcf7multistepsection h3.mgswpcf7multistepsection-title em {
    display: none;
}
</pre>
				<hr />
				<img src="<?php echo esc_url( plugins_url( 'images/ready-to-start.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'percentage display', 'mgscformsevenmultistep'); ?>" width="97%" />
				<p><span class="yellowbg"><?php esc_html_e( '[You can add in your theme style css]', 'mgscformsevenmultistep'); ?></span></p>
				<p class="note"><?php esc_html_e( 'Add the following css in your theme style css to hide the above Ready to Start! Button', 'mgscformsevenmultistep'); ?></p>
<pre>
.wpcf7-form .mgswpcf7multistep-btn-group.mgswpcf7multistep-btn-box-1 .btn.mgswpcf7btn-default.disable {
    display: none;
}
</pre>

			</div>
					
		</div>
		
		<hr />
			
		<?php require_once(MGS_CFORMSEVENMULTISTEP_PLUGIN_DIR . '/inc/mgscformsevenmultistep-sidebar.php'); ?>
		
	</div>
</div>
